<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Mlp642IncreaseTotalUnitsSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->float('new_total',10,2)->after('total');
        });

        DB::update('update orders set new_total = total');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('new_total','total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->float('new_total',8,2)->after('total');
        });

        DB::update('update orders set new_total = total');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('new_total','total');
        });
    }
}
