<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSellerReferenceColumnAndUpdateSomeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->text('terms_of_sales_id')->nullable()->change();
            $table->renameColumn('terms_of_sales_id', 'terms_of_sales');
            $table->text('seller_reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->string('terms_of_sales')->nullable()->change();
            $table->renameColumn('terms_of_sales', 'terms_of_sales_id');
            $table->dropColumn('seller_reference');
        });
    }
}
