<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubjectIntoOrderGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_group', function (Blueprint $table) {
            $table->string('subject');
            $table->string('note')->nullable();
            $table->float('total_amount')->default(0);
            $table->integer('order_total_qty')->default(0);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('subject');
            $table->dropColumn('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('subject');
            $table->string('note')->nullable();
        });

        Schema::table('order_group', function (Blueprint $table) {
            $table->dropColumn('subject');
            $table->dropColumn('note');
            $table->dropColumn('total_amount');
            $table->dropColumn('order_total_qty');
        });
    }
}
