<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('label');
            $table->string('name');
            $table->string('description');
            $table->timestamps();

            $table->index('uuid');
        });

        Schema::create('order_group', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('id_readable');
            $table->string('author_id');
            $table->string('recipient_author_id');

            $table->integer('order_status_id')->unsigned();
            $table->integer('order_group_id')->nullable()->unsigned();
            $table->float('items_total');
            $table->integer('items_total_qty');
            $table->float('total');

            $table->string('subject');
            $table->string('note')->nullable();
            $table->string('shipment_port_name')->nullable();
            $table->string('locality')->nullable();
            $table->string('payment_terms')->nullable();
            $table->string('terms_of_sales_id')->nullable();
            $table->string('external_reference_id')->nullable();
            $table->string('po_reference')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('channel_id')->nullable();
            $table->string('country_id')->nullable();

            $table->integer('weight_type_id')->unsigned()->nullable();
            $table->timestamp('delivered_at')->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->string('delivery_term_id')->nullable();
            $table->string('delivery_method_id')->nullable();
            $table->string('delivery_postcode')->nullable();
            $table->string('delivery_address')->nullable();
            $table->integer('billing_address_id')->unsigned()->nullable();

            $table->timestamps();

            $table->index('uuid');
            $table->index('id_readable');
            $table->index('author_id');
            $table->index('channel_id');
            $table->index(['author_id', 'channel_id']);

            $table->foreign('order_status_id')
                ->references('id')->on('order_statuses')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('order_group_id')
                ->references('id')->on('order_group')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('quantity');
            $table->float('unit_price');
            $table->float('unit_total');
            $table->string('product_id');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function($table) {
            $table->dropForeign(['order_id']);
        });
        Schema::dropIfExists('order_items');

        Schema::table('orders', function($table) {
            $table->dropForeign(['order_status_id']);
            $table->dropForeign(['order_group_id']);
        });
        Schema::dropIfExists('orders');

        Schema::dropIfExists('order_statuses');

        Schema::dropIfExists('order_group');
    }
}
