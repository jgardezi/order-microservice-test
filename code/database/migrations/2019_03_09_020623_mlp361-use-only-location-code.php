<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Mlp361UseOnlyLocationCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function(Blueprint $table){
           $table->string('location_code');
        });

        DB::statement("UPDATE order_items SET location_code = JSON_UNQUOTE(JSON_EXTRACT(location,'$.code'));");

        Schema::table('order_items', function(Blueprint $table){
            $table->dropColumn('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function(Blueprint $table){
            $table->json('location');
        });

        DB::statement("UPDATE order_items SET location = JSON_SET('{\"code\":\"\",\"name\":\"\"}','$.code',location_code,'$.name','To be fixed later');");

        Schema::table('order_items', function(Blueprint $table){
            $table->dropColumn('location_code');
        });
    }
}
