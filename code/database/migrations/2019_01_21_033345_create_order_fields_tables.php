<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFieldsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('label', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->integer('sort')->unsigned();
            $table->timestamps();
        });

        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('label', 191)->nullable();
            $table->string('code', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->integer('sort')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_fields_tables');
    }
}
