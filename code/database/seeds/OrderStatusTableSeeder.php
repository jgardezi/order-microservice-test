<?php

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Entities\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();
        $data = [
            [
                'uuid' => Str::uuid(),
                'label' => 'draft',
                'name' => 'Draft',
                'description' => 'Description for Draft.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'uuid' => Str::uuid(),
                'label' => 'offer',
                'name' => 'Offer',
                'description' => 'Description for Offer.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'uuid' => Str::uuid(),
                'label' => 'pending',
                'name' => 'Pending Order',
                'description' => 'Description for Pending.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'uuid' => Str::uuid(),
                'label' => 'confirmed',
                'name' => 'Confirmed Order',
                'description' => 'Description for Confirmed Order.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'uuid' => Str::uuid(),
                'label' => 'closed',
                'name' => 'Closed Order',
                'description' => 'Description for Closed Order.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'uuid' => Str::uuid(),
                'label' => 'cancelled',
                'name' => 'Cancelled Order',
                'description' => 'Description for Cancelled Order.',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ];

        foreach ($data as $datum) {
            $existing = OrderStatus::where('label',$datum['label'])->first();
            if ($existing) {
                $existing->update($datum);
            } else {
                OrderStatus::insert($datum);
            }
        }
    }
}
