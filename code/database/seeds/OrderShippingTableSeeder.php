<?php

use Illuminate\Database\Seeder;

use App\Entities\Shipping;

class OrderShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Shipping
        $shippings = [
            [
                "name" => "Sea Freight",
            ],
            [
                "name" => "Air Freight",
            ],
            [
                "name" => "Road Freight",
            ],
        ];

        DB::transaction(function () use ($shippings) {
            foreach ($shippings as $shipping) {
                $name = $shipping['name'];
                $label = str_slug($name);
                $shipping['label'] = $label;
                $shipping['description'] = "Description for " . $name . '.';
                $shipping['sort'] = 0;

                $exist = Shipping::where('label', '=', $label)->first();

                if (!empty($exist)) {
                    if ($exist->name != $name || $exist->description != $shipping['description'] || $exist->sort != $shipping['sort']) {
                        Shipping::find($exist->id)->update($shipping);
                    }
                } else {
                    $shippingInstance = new Shipping;
                    $shippingInstance->name = $name;
                    $shippingInstance->label = $label;
                    $shippingInstance->description = $shipping['description'];
                    $shippingInstance->sort = $shipping['sort'];
                    $shippingInstance->save();
                }
            }
        });
    }
}
