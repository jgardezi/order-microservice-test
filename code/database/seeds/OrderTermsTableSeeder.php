<?php

use Illuminate\Database\Seeder;
use App\Entities\Term;

class OrderTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $terms = [
            ['Ex Works', 'EXW'],
            ['Free Carrier', 'FCA'],
            ['Carriage Paid To', 'CPT'],
            ['Carriage and Insurance Paid to', 'CIP'],
            ['Delivered At Terminal', 'DAT'],
            ['Delivered At Place', 'DAP'],
            ['Delivered Duty Paid', 'DDP'],
            ['Free Alongside Ship', 'FAS'],
            ['Free on Board', 'FOB'],
            ['Cost and Freight', 'CFR'],
            ['Cost, Insurance & Freight', 'CIF'],
            ['Free At Terminal', 'FAT'],
        ];

        foreach($terms as $i => $term):
            $termItem = [
                'id' => $i + 1,
                'name' => $term[0],
                'label' => str_slug($term[0]),
                'code' => $term[1],
                'description' => 'Description for ' . $term[0] . '.',
                'sort' => 0,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ];

            $exist = Term::where('label', '=', $termItem['label'])->first();
            if(!$exist) {
                DB::table('terms')->insert($termItem);
            }

        endforeach;
    }
}
