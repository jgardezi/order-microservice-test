<?php

namespace App\Providers;

use App\Entities\Orders;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::channel('orders.{orgId}.created', function ($user, $orgId) {
            return $user->org_id === $orgId;
        });

        Broadcast::channel('orders.{orderID}.updated', function ($user, $orderID) {
            $order = Orders::find((int)$orderID);

            if (is_null($order)) {
                return false;
            }
            return (
                $user->id === $order->author_id
//                || (/* TODO: $user->is_manager && */)
            );
        });
    }
}

