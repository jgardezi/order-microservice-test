<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot() {
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // @todo This needs to be tested.
        //$this->app->bind(Manager::class, function($app) {
        //    $manager = new Manager;
        //
        //    // Use the serializer of your choice.
        //    $manager->setSerializer(new JsonApiSerializer());
        //    //$manager->setSerializer(new ArraySerializer());
        //    return $manager;
        //});
    }
}
