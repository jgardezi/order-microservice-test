<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /* */
        \App\Events\AssignChannelToOrderEvent::class => [
            \App\Listeners\AssignChannelToOrderListener::class
        ],

        \App\Events\AssignPXCReferenceIDToOrderEvent::class => [
            \App\Listeners\AssignPXCReferenceIDToOrderListener::class
        ]
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        \App\Listeners\OrderEventSubscriber::class,
        \App\Listeners\OrderGroupEventSubscriber::class
    ];
}