<?php

namespace App\Providers;

use App\Supports\RequestBodyDocumentSerializer;
use App\Supports\ResponseDocumentSerializer;
use App\Supports\ResponseErrorsParser;
use App\Supports\ResponseParser;
use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use PXC\JsonApi\Client\ClientRequest;
use PXC\JsonApi\Client\MachineRequest;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use PXC\JsonApi\Client\MachineRequestInterface;
//use Swis\JsonApi\Client\ItemDocumentSerializer;

use PXC\JsonApi\Client\ClientRequestInterface;
use Swis\JsonApi\Client\Interfaces\ParserInterface;
use Swis\JsonApi\Client\Interfaces\ClientInterface as ApiClientInterface;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;



class PXCApiClientServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            app()->basePath() . '/vendor/pxc/json-api-client/config/jsonapi.php',
            'jsonapi'
        );

        $this->app->bind(ClientRequestInterface::class, function($app) {
            return new ClientRequest(
                $app->make(ApiClientInterface::class),
                new RequestBodyDocumentSerializer(),
                $app->make(ResponseParser::class)
            );
        });

        $this->app->bind(MachineRequestInterface::class, function ($app) {
            return new MachineRequest(
                $app->make(ApiClientInterface::class),
                new RequestBodyDocumentSerializer(),
                $app->make(ParserInterface::class)
            );
        });
    }

    protected function getHttpClient(): HttpClient
    {
        return HttpClientDiscovery::find();
    }

    protected function getMessageFactory(): MessageFactory
    {
        return MessageFactoryDiscovery::find();
    }
}