<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidatorsProvider extends ServiceProvider
{
    protected $classes = [
        'createOrder'       => \App\Http\Validators\CreateOrder::class,
        'queryOrder'        => \App\Http\Validators\QueryOrder::class,
        'updateOrder'       => \App\Http\Validators\UpdateOrder::class,
        'duplicateOrder'    => \App\Http\Validators\DuplicateOrder::class,
        'deleteOrder'       => \App\Http\Validators\DeleteOrder::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('requiredField', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $field = array_shift($parameters);
            $keys = explode('.', $field);
            $fieldValue = $data;

            foreach ($keys as $key) {
                if (is_array($fieldValue) && isset($fieldValue[$key])) {
                    $fieldValue = $fieldValue[$key];
                }
            }

            if (in_array($fieldValue, $parameters)) {
                if (empty($value)) {
                    $validator->errors()->add($attribute, "The $attribute field is required when $field is $fieldValue.");
                    return false;
                }
            }
            return true;
        }, 'The :attribute is required.');

        Validator::extend('requiredIfExists', function ($attribute, $value, $parameters, $validator) {
            return !empty($value);
        }, 'The :attribute is required.');

        Validator::extend('date', function ($attribute, $value, $parameters, $validator) {
            $date = \DateTime::createFromFormat('Y-m-d', $value);
            return $date;
        }, 'The :attribute is invalid.');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->classes as $key => $clazz) {
            $this->app->singleton('validators.'.$key, $clazz);
        }
    }

}
