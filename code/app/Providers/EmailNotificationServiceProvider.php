<?php

namespace App\Providers;

use App\Services\EmailQueueClient;
use App\Services\EmailQueueClientInterface;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class EmailNotificationServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->app->bind(EmailQueueClientInterface::class, function() {
           return new EmailQueueClient();
        });
    }
}

