<?php

namespace App\Providers;

use App\Services\AuthService;

class PXCAuthServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->singleton(AuthService::class, function () {
            return new AuthService();
        });
    }
}