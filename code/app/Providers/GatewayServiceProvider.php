<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GatewayServiceProvider extends ServiceProvider
{
    protected $gateways = [
        \App\Gateways\StockGateway::class,
        \App\Gateways\ProductGateway::class,
        \App\Gateways\UserGateway::class,
    ];

    public function boot() {
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->gateways as $gateway) {
            $this->app->singleton($gateway);
        }
    }
}
