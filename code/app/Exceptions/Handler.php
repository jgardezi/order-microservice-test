<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            // We've already formatted this to JSON+API spec so just pass it on.
            // See code/app/Http/Requests/JsonApiRequestAbstract.php:50
            return $exception->response;
        }

        if ($exception instanceof BadHttpRequestException) {
            // A controller, gateway, or repository encountered an issue with the request.
            return $exception->renderToJson();
        }

        // Boring production message.
        $productionMessage = "There was an problem processing your request";

        // Generic JSON+API compliant
        $error = [
            'code' => 500,
            'detail' => $productionMessage,
        ];

        if (env('APP_DEBUG')) {
           $error['file'] = $exception->getFile() . ':' . $exception->getLine();
           $error['stack'] = explode("\n",$exception->getTraceAsString());
           $error['detail'] = $exception->getMessage();
        }

        return new Response([
            'meta' => [
                'message' => $productionMessage,
                'status' => 'failed',
            ],
            'errors' => [
                0 => $error,
            ]
        ], 500);
    }
}
