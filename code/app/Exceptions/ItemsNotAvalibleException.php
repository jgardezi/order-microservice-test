<?php


namespace App\Exceptions;
use Illuminate\Http\Response;


class ItemsNotAvalibleException extends BadHttpRequestException
{
    protected $items;

    public function __construct(array $items)
    {
        parent::__construct('', 412);
        $this->items = $items;
    }

    public function renderToJson()
    {
        $ret = [
            'meta' => [
                'message' => 'Item not available.',
                'status' => 412,
            ],
            'errors' => []
        ];
        foreach ($this->items as $item) {
            if ($item->available === false) {
                $ret['errors'][] = [
                    'detail' => 'Item not available. Missing quantity: ' . $item->missing,
                    'item' => $item,
                ];
            }
        }
        return new Response($ret);
    }
}