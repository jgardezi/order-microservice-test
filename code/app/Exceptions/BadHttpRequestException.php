<?php

namespace App\Exceptions;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;

class BadHttpRequestException extends HttpException
{
    public function __construct(string $message, int $statusCode = 400)
    {
        parent::__construct($statusCode, $message, null,[], 0);
    }

    /**
     * @return Response
     */
    public function renderToJson()
    {
        // JSON+API compliant
        $error = [
            'code' => $this->getStatusCode(),
            'detail' => $this->getMessage(),
        ];

        if (env('APP_DEBUG')) {
            $error['file'] = $this->getFile() . ':' . $this->getLine();
        }

        return new Response([
            'meta' => [
                'message' => $this->getMessage(),
                'status' => 'failed',
            ],
            'errors' => [
                0 => $error,
            ]
        ], $this->getStatusCode());
    }
}