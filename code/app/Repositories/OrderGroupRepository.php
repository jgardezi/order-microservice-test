<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderGroupRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderGroupRepository extends RepositoryInterface
{
    //
    /**
     * Create order group
     * @param array $orders
     * @return mixed
     */
    function createOrderGroup(array $orders = []);
}
