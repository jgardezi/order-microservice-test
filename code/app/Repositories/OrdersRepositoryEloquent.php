<?php

namespace App\Repositories;

use App\Entities\OrderAttachment;
use App\Entities\OrderGroup;
use App\Entities\OrderItem;
use App\Entities\OrderStatus;
use App\Events\AssignChannelToOrderEvent;
use App\Events\AssignPXCReferenceIDToOrderEvent;
use App\Events\OrderCreated;
use App\Events\OrderStatusChanged;
use App\Events\OrderUpdated;
use App\Events\OrganisationGeneralEvent;
use App\Gateways\StockGateway;
use App\Presenters\OrderPresenter;
use App\Repositories\traits\Updatable;
use App\Supports\ExternalData;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Orders;
use PXC\JsonApi\Client\MachineRequestInterface;
use App\Gateways\UserGateway;

/**
 * Class OrdersRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdersRepositoryEloquent extends BaseRepository implements OrdersRepository
{
    use Updatable;

    protected $client;
    protected $stock;
    protected $users;

    protected $fieldSearchable = [
        'channel_id'
    ];

    public function __construct(MachineRequestInterface $client, StockGateway $stock, UserGateway $users)
    {
        $this->client = $client;
        $this->stock = $stock;
        $this->users = $users;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        parent::__construct(app());
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Orders::class;
    }

    public function presenter()
    {
        return OrderPresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $attributes
     * @return Orders|mixed
     * @throws \Exception
     */
    public function createOrder(array $attributes)
    {
        DB::beginTransaction();
        try {
            // 0. Prepare data
            $data = Orders::fromCreateData($attributes);
            // 1. new order instance
            $order = $this->skipPresenter()->create($data);

            // 1.1. Save order group
            $group = OrderGroup::create(OrderGroup::fromCreateData($attributes));
            $order->group()->associate($group);

            // 1.2. Save order
            $order->save();

            // 1.3. Calculate order group totals
            $group->calculateTotals()->save();

            // 2. save order items

            $items = array_map(function ($item) {
                return new OrderItem($item);
            }, $data['items']);
            
            $order->items()->saveMany($items);

            // 3. Save order attachments
            if (isset($data['attachments']) && sizeof($data['attachments']) > 0) {
                $attachments = [];
                foreach ($data['attachments'] as $item) {
                    $attachments[] = new OrderAttachment($item);
                }
                $order->attachments()->saveMany($attachments);
            }

            // If not cached already - grab all users at once.
            $send_to_users = $order->send_to_users;
            if (!$send_to_users) {
                $send_to_users = [];
            }
            $this->users->findByIds(array_merge([
                $order->author_id,
                $order->recipient_author_id
            ], $send_to_users));

            $author = $this->users->findById($order->author_id);

            $order->org_id = $author['organisation']['id'];
            $order->save();
            
            // 3. Commit
            DB::commit();

            // 4. Dispatch order updated event
            $presenter = app(OrderPresenter::class);
            $payload = $presenter->present($order);

            $authorOrganisationId = $author['organisation']['id'];
            $recipientAuthor = $this->users->findById($order->recipient_author_id);
            $recipientAuthorOrganisationId = $recipientAuthor['organisation']['id'];
            // TODO, remove items if exist to reduce payload size
            event(new OrganisationGeneralEvent($authorOrganisationId, $payload, OrderCreated::class));
            if($recipientAuthorOrganisationId && $recipientAuthorOrganisationId !== "") {
                event(new OrganisationGeneralEvent($recipientAuthorOrganisationId, $payload, OrderCreated::class));
            }
            return $this->skipPresenter(false)->parserResult($order);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    public function createDraftOrder(array $attributes) {

    }

    /**
     * @param array $attributes
     * @param $order
     * @param bool $skipPresenter
     * @return mixed
     * @throws \Exception
     */
    public function updateOrder($order, array $attributes, $skipPresenter = false) {
        DB::beginTransaction();
        try {
            // 0. Prepare data
            $data = Orders::fromUpdateData($attributes);

            // 1. Find and update order
            if (is_string($order)) {
                $order = $this->skipPresenter()->findWhereFirst(['uuid' => $order]);
            }
            $order->load(['status', 'items', 'attachments']);
            $oldState = $order->status->label;
            $order->update($data);

            // 1.1. update group
            $order->group->update(OrderGroup::fromUpdateData($data));
            $order->group->save();

            $attachmentAdded = false;
            if (isset($data['attachments']) && sizeof($data['attachments']) > 0) {
                $attachments = [];
                foreach ($data['attachments'] as $item) {
                    $attachment = $order
                        ->attachments()
                        ->where('attachment_id', '=', $item['attachment_id'])
                        ->first();
                    if (!$attachment) {
                        $attachments[] = new OrderAttachment($item);
                        $attachmentAdded = true;
                    }
                }
                $order->attachments()->saveMany($attachments);
            }

            $updates = $order->getDirty();
            
            // 4. Saving
            $order->save();

            // Refresh model
            $order->refresh();

            $newState = $order->status->label;

            if ($oldState !== $newState) {
                if ( $newState == OrderStatus::PENDING ) {
                    $availability = $this->stock->checkStockAvailability($order);

                    if (isset($availability['errors'])) {
                        DB::rollback();
                        return [
                            'errors' => [
                                'status' => 412,
                                0 => [
                                    'detail' => 'There was a problem getting stock availability.',
                                    'errors' => $availability['errors'],
                                ]
                            ]
                        ];
                    }
                    if ($availability->allAvailable === false) {
                        DB::rollback();
                        $ret = [
                            'errors' => [
                                'status' => 412,
                            ]
                        ];
                        foreach ($availability->items as $item) {
                            if ($item->available === false) {
                                $ret['errors'][] = [
                                    'detail' => 'Item not available. Missing quantity: ' . $item->missing,
                                    'item' => $item,
                                ];
                            }
                        }
                        return $ret;
                    }
                }
                if (
                    in_array( $oldState, [ OrderStatus::PENDING, OrderStatus::CONFIRMED, OrderStatus::CLOSED] ) &&
                    in_array( $newState, [ OrderStatus::CANCELLED, OrderStatus::OFFER] )
                ) {
                    $ret = $this->stock->returnStock($order);
                    if (isset($ret['errors'])) {
                        $ret['errors']['status'] = 400;
                        return $ret;
                    }
                }
            }

            // 5. Commit
            DB::commit();

            $userId = Request::user()->id;

            if ($oldState !== $newState) {
                Event::dispatch(new OrderStatusChanged($order, $oldState, $newState, $userId));
            }

            // 6. Dispatch order updated event
            if( !empty($updates) || $attachmentAdded ) {
                Event::dispatch(new OrderUpdated($order, $updates, $userId));
            }
            return $this->skipPresenter($skipPresenter)->parserResult($order);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    public function updateDraftOrder($order, array $attributes) {
        DB::beginTransaction();
        try {
            if (is_string($order)) {
                $order = $this->skipPresenter()->findWhereFirst(['uuid' => $order]);
            }
            $oldState = $order->status->label;
            $order = $this->updateOrder($order, $attributes, true);
            if (isset($order['errors'])) {
                return $order;
            }
            $newState = $order->status->label;

            if (
                $oldState === OrderStatus::DRAFT
                && ($newState === OrderStatus::PENDING || $newState === OrderStatus::OFFER)
            ) {
                $recipients = $order->send_to_users;
                // update order before duplicate;
                $order->setAttribute('send_to_users', null);
                $order->setAttribute('recipient_author_id', array_pop($recipients));

                // duplicate order
                if (sizeof($recipients) > 0) {
                    foreach ($recipients as $userId) {
                        $this->duplicate($order, [ 'recipient_author_id' => $userId ],true);
                    }
                }

                // Generate PXCReferenceID (id_readable) when author convert draft => offer/order
                Event::dispatch(new AssignPXCReferenceIDToOrderEvent($order));
                $order->save();
                $group = $order->group;
                Event::dispatch(new AssignChannelToOrderEvent($group));
                foreach ($group->orders()->get() as $order) {
                    Event::dispatch(new OrderCreated($order));
                }
                // TODO: this is a hack for letting counter party see the created order
                // we should improve the logic and performance
                $presenter = app(OrderPresenter::class);
                $payload = $presenter->present($order);
                $payload["data"]["id"] = $order->uuid;
                $author = $this->users->findById($order->author_id);
                $authorOrganisationId =$author['organisation']['id'];
                $recipientAuthor = $this->users->findById($order->recipient_author_id);
                $recipientAuthorOrganisationId = $recipientAuthor['organisation']['id'];
                event(new OrganisationGeneralEvent($authorOrganisationId, $payload["data"], OrderCreated::class));
                if($recipientAuthorOrganisationId && $recipientAuthorOrganisationId !== "") {
                    event(new OrganisationGeneralEvent($recipientAuthorOrganisationId, $payload["data"], OrderCreated::class));
                }
                // The job might reduce a lot of response time, but I can not make it work?
                // dispatch(new OrderCreatedJob($order));
            }
            DB::commit();

            // Refresh model
            $order->refresh();

            return $this->skipPresenter(false)->parserResult($order);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }

    }

    /**
     * Find all user's orders
     * @param $userID
     * @param int $limit
     * @return mixed
     */
    public function findUserOrders($userID, $limit = 10)
    {
        $this->applyConditions(['author_id' => $userID]);
        // Skip Criteria. because this is global APIs
        // Only use for manager user.
        $this->skipCriteria(true);
        return $this->paginate($limit);
    }

    /**
     * Find first order
     * @param $where
     * @param array $columns
     * @param bool $first
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findWhereFirst($where, $columns = ['*'], $first = true) {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);
        if ($first) {
            $model = $this->model->first($columns);

            if ($model) {
                $groups = [ $model->group ];
                app(ExternalData::class)->loadExternalData(Collection::wrap($groups));
            }
        }
        else {
            $model = $this->model->get($columns);

            if ($model) {
                // Get all order groups
                $groups = [];
                foreach ($model as $order) {
                    $groups[] = $order->group;
                }
                app(ExternalData::class)->loadExternalData(Collection::wrap($groups));
            }
        }
        $this->resetModel();

        if ($model) {
            return $this->parserResult($model);
        }
    }

    /**
     * Find team member IDs who has orders
     * @param $orgId
     * @param $excludes
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findTeamMemberIds(string $orgId, $excludes = []): array
    {
        $this->applyCriteria();
        $this->applyScope();

        $ids = [];
        // Maybe your team members is author
        $authors = $this->model
            ->where('org_id', $orgId)
            ->whereNotIn('author_id', $excludes)
            ->groupBy('author_id')
            ->selectRaw('count(id) as count, author_id')
            ->get();

        foreach ($authors as $author) {
            $ids[] = $author->author_id;
        }

        $this->resetModel();
        return $ids;
    }

    /**
     * @param $order
     * @param $attributes
     * @return bool|mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function duplicate($order, array $attributes = null, $skipPresenter = false) {
        $attributes = $attributes ? $attributes : ['order_status_id' => OrderStatus::draft()];
        if (is_string($order)) {
            $order = $this->skipPresenter()->findWhereFirst(['uuid' => $order]);
        }

        // 1. Replicate order items.
        $items = [];
        foreach ($order->items as $item) {
            $items[] = $item->replicate();
        }
        // 2. Replicate order.
        $order = $order->replicate();
        foreach ($attributes as $attribute => $value) {
            $order->setAttribute($attribute, $value);
        }

        // Get a new uuid
        $order->uuid = Str::uuid();

        // 3. save order
        $order->save();

        // 4. save order items
        $order->items()->saveMany($items);

        Event::dispatch(new AssignPXCReferenceIDToOrderEvent($order));

        return $this->skipPresenter($skipPresenter)->parserResult($order);
    }

    public function addAttachments($order, array $data = [], $skipPresenter = false) {
        DB::beginTransaction();
        try {
            if (is_string($order)) {
                $order = $this->skipPresenter()->findWhereFirst(['uuid' => $order]);
            }

            $attachments = [];
            foreach ($data as $item) {
                $attachment = $order
                    ->attachments()
                    ->where('attachment_id', '=', $item['attachmentId'])
                    ->first();

                if (!$attachment) {
                    $attachments[] = new OrderAttachment([
                        'user_id'       => $item['userId'],
                        'attachment_id' => $item['attachmentId']
                    ]);
                }

            }
            $order->attachments()->saveMany($attachments);
            DB::commit();
            return $this->skipPresenter($skipPresenter)->parserResult($order);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    public function deleteAttachments($order, array $attachmentIds = [], $skipPresenter = false) {
        DB::beginTransaction();
        try {
            if (is_string($order)) {
                $order = $this->skipPresenter()->findWhereFirst(['uuid' => $order]);
            }

            $order->attachments()
                ->whereIn('attachment_id', $attachmentIds)
                ->delete();

            DB::commit();
            return $this->skipPresenter($skipPresenter)->parserResult($order);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    /**
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function summary() {
        $this->applyCriteria();
        $this->applyScope();

        $data = [];
        $query = $this->model;

        $states = [
            OrderStatus::DRAFT,
            OrderStatus::OFFER,
            OrderStatus::PENDING,
            OrderStatus::CONFIRMED,
            OrderStatus::CLOSED,
            OrderStatus::CANCELLED,
        ];

        foreach ($states as $state) {
            $data[$state] = (clone $query)
                ->where('order_status_id', OrderStatus::getByLabel($state)->id)
                ->count();
        }

        $data['all'] =
            $data[OrderStatus::DRAFT] +
            $data[OrderStatus::OFFER] +
            $data[OrderStatus::PENDING] +
            $data[OrderStatus::CONFIRMED];

        $this->resetModel();
        return [
            'data' => $data
        ];
    }

}
