<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/10/19
 * Time: 5:38 PM
 */

namespace App\Repositories\traits;


trait Updatable
{
    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     */
    public function update(array $attributes, $id) {
        foreach ( $attributes as $key => $value) {
            if (!$this->model->isUpdatable($key)) {
                unset($attributes[$key]);
            }
        }
        return $this->update($attributes, $id);
    }
}