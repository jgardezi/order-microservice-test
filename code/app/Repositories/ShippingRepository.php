<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ShippingRepository.
 *
 * @package namespace App\Repositories;
 */
interface ShippingRepository extends RepositoryInterface
{
    //
}
