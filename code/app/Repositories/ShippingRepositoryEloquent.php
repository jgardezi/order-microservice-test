<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ShippingRepository;
use App\Entities\Shipping;
use App\Validators\ShippingValidator;

/**
 * Class ShippingRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ShippingRepositoryEloquent extends BaseRepository implements ShippingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Shipping::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
