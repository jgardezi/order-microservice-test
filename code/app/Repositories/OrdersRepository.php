<?php

namespace App\Repositories;

use App\Entities\Orders;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdersRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdersRepository extends RepositoryInterface
{
    /**
     * Create new an order, also attach the order items
     * @param array $attributes
     * @return Orders
     */
    public function createOrder(array $attributes);

    /**
     * Update an order
     *
     * @param $order
     * @param array $attributes
     * @param bool $skipPresenter
     * @return mixed
     */
    public function updateOrder($order, array $attributes, $skipPresenter = false);

    /**
     * Update an draft order
     *
     * @param $order
     * @param array $attributes
     * @return mixed
     */
    public function updateDraftOrder($order, array $attributes);

    /**
     * Global API find user's orders
     * @param $userID
     * @param int $limit
     * @return mixed
     */
    public function findUserOrders($userID, $limit = 10);

    /**
     * Find member ids who has order in organisation.
     * @param string $orgId
     * @param array $excludes
     * @return array
     */
    public function findTeamMemberIds(string $orgId, $excludes = []): array;

    /**
     * Duplicate an order
     *
     * @param $order
     * @param array|null $attributes
     * @param bool $skipPresenter
     * @return mixed
     */
    public function duplicate($order, array $attributes = null, $skipPresenter = false);

    /**
     * Order summary
     *
     * @return mixed
     */
    public function summary();

    /**
     * Find where first
     *
     * @param $where
     * @param array $columns
     * @param bool $first
     * @return mixed
     */
    public function findWhereFirst($where, $columns = ['*'], $first = true);
}
