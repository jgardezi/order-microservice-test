<?php

namespace App\Repositories;

use App\Entities\OrderAttachment;
use App\Entities\OrderGroup;
use App\Entities\OrderItem;
use App\Entities\Orders;
use App\Events\AssignChannelToOrderEvent;
use App\Events\AssignPXCReferenceIDToOrderEvent;
use App\Events\OrderGroupCreated;
use App\Gateways\StockGateway;
use App\Gateways\UserGateway;
use App\Presenters\OrderGroupPresenter;
use App\Supports\ExternalData;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

/**
 * Class OrderGroupRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderGroupRepositoryEloquent extends BaseRepository implements OrderGroupRepository
{
    protected $order;
    protected $userGateway;
    protected $stockGateway;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderGroup::class;
    }

    public function presenter()
    {
        return OrderGroupPresenter::class;
    }

    public function __construct(OrdersRepository $order, UserGateway $userGateway, StockGateway $stockGateway)
    {
        $this->order = $order;
        $this->userGateway = $userGateway;
        $this->stockGateway = $stockGateway;
        parent::__construct(app());
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createOrderGroup(array $orders = []) {
        DB::beginTransaction();
        try {
            $data = array_get($orders, 0);
            // 0. Create new a group
            $group = $this->skipPresenter()
                ->create(
                    OrderGroup::fromCreateData([
                        'note'      => $data['note'],
                        'subject'   => $data['subject']
                    ])
                );

            // 1. Saving orders
            foreach ($orders as $order) {
                $data = Orders::fromCreateData($order);

                $order = new Orders($data);

                $order->group()->associate($group);

                $items = [];
                foreach ($data['items'] as $item) {
                    $items[] = new OrderItem($item);
                }

                $attachments = [];
                if (isset($data['attachments'])) {
                    foreach ($data['attachments'] as $item) {
                        $attachments[] = new OrderAttachment($item);
                    }
                }

                $author = $this->userGateway->findById($order->author_id);

                $order->org_id = $author['organisation']['id'];

                // Saving order
                $order->save();

                // Saving order items
                $order->items()->saveMany($items);

                if (sizeof($attachments) > 0) {
                    $order->attachments()->saveMany($attachments);
                }
            }

            // 2. Calculate order group totals
            $group->calculateTotals()->save();

            Event::dispatch(new AssignPXCReferenceIDToOrderEvent($group));

            Event::dispatch(new AssignChannelToOrderEvent($group));
            Event::dispatch(new OrderGroupCreated($group, $data['org_uuid']));
            DB::commit();
            return $this->skipPresenter(false)->parserResult($group);
        }
        catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        $this->applyCriteria();
        $this->applyScope();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;
        $results = $this->model->{$method}($limit, $columns);

        // Load all users and stock in one go.
        if (!empty($results)) {
            app(ExternalData::class)->loadExternalData($results);
        }
        $results->appends(app('request')->query());
        $this->resetModel();

        return $this->parserResult($results);
    }

}
