<?php

namespace App\Entities;

use App\Entities\Traits\CustomCasts;
use App\Entities\Traits\Updatable;

class OrderGroup extends OrdersModel
{
    use Updatable, CustomCasts;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_group';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject',
        'note',
    ];

    /**
     * The attributes that you can update.
     * @var array
     */
    protected $updatable = [
        'subject',
        'note',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'subject'   => 'text',
        'note'      => 'text',
    ];

    /**
     * Get the orders for the `order group`.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

    public function calculateTotals() {
        $orders = $this->orders()->get(['total']);
        $totalAmount = 0;
        foreach ($orders as $order) {
            $totalAmount += $order->total;
        }
        $this->total_amount = $totalAmount;
        $this->order_total_qty = sizeof($orders);
        return $this;
    }

    /**
     * @param array $attributes
     * @return array
     */
    public static function fromCreateData(array $attributes) {
        return [
            'subject'       => array_get($attributes,'subject', null),
            'note'          => array_get($attributes,'note', null),
        ];
    }

    /**
     * @param array $attributes
     * @return array
     */
    public static function fromUpdateData(array $attributes) {
        $data = [];
        $attrs = (new self())->getUpdatable();
        foreach ($attrs as $key) {
            if (isset($attributes[$key])) {
                $data[$key] = $attributes[$key];
            }
        }
        return $data;
    }
}