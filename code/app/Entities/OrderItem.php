<?php

namespace App\Entities;

use App\Entities\Traits\Updatable;
use App\Supports\Convention;

/**
 * Class OrderStatus.
 *
 * @package namespace App\Entities;
 */
class OrderItem extends OrdersModel
{
    use Updatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'quantity',
        'unit_price',
        'product_id',
        'unit_total',
        'location_code',
        'production_dates',
    ];

    protected $updatable = [
        'quantity',
        'location_code',
        'production_dates',
        'unit_price',
    ];

    /**
     * The attributes that will hidden.
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'production_dates' => 'array'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return $this
     */
    public function calculateTotals() {
        $this->unit_total = ($this->unit_price * $this->quantity);
        return $this;
    }

    /**
     * Parse create data to model attributes
     * @param array $data
     * @return array
     */
    public static function fromCreateData(array $data = []) {
        $attributes = [
            'quantity'          => $data['quantity'],
            'product_id'        => $data['productId'],
            'unit_price'        => array_get($data,'price',0),
            'unit_total'        =>
                (array_get($data,'price',0) * $data['quantity']),
            'location_code'     => array_get($data, 'locationCode'),
        ];

        if (isset($data['productionDate'])) {
            $attributes['production_dates'] = [ $data['productionDate'] ];
        }

        return $attributes;
    }

    /**
     * Parse create data to model attributes
     * @param array $data
     * @return mixed
     */
    public static function fromUpdateData(array $data = []) {
        $attributes = $data['attributes'];
        $attributes['id'] = array_get($data, 'id', null);
        if (isset($data['delete'])) {
            $attributes['delete'] = $data['delete'];
        }
        return Convention::snakeConvention($attributes);
    }
}
