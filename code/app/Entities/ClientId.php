<?php


namespace App\Entities;

use Illuminate\Support\Facades\DB;

class ClientId extends OrdersModel
{
    public function scopeOrgId($query,$uuid)
    {
        return $query->where('org_uuid', $uuid);
    }

    /**
     * Get seed and increment atomically.
     */
    public function atomicBump()
    {
        $id = $this->id;
        DB::update("UPDATE client_ids SET seed = (@new_val := seed) + 1 WHERE id = $id;");
        $seed = DB::select("SELECT @new_val as seed")[0]->seed;

        return $seed;
    }

}