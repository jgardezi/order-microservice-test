<?php

namespace App\Entities;

use App\Entities\Traits\CustomCasts;
use App\Entities\Traits\Updatable;
use App\Exceptions\BadHttpRequestException;
use App\Supports\Convention;
use App\Supports\Helpers;
use Ramsey\Uuid\Uuid;

/**
 * Class Orders.
 *
 * @package namespace App\Entities;
 */
class Orders extends OrdersModel
{
    use Updatable, CustomCasts;

    /** Update updated_at on the associated order group when the order is updated
     *  so that sorting works.
     */
    protected $touches = ['group'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
//        'id_readable',
        'author_id',
        'org_id',
        'recipient_author_id',
        'order_status_id',
        'items_total',
        'items_total_qty',
        'total',
        'shipment_port_name',
        'weight_type_id',
        'channel_id',
        'delivered_at',
        'delivery_date',
        'delivery_address',
        'delivery_term_id',
        'delivery_method_id',
        'delivery_postcode',
        'billing_address_id',
        'locality',
        'country_id',
        'payment_terms',
        'terms_of_sales',
        'external_reference_id',
        'po_reference',
        'currency_code',
        'send_to_users',
        'seller_reference',
    ];

    /**
     * The attributes that you can update.
     * @var array
     */
    protected $updatable = [
//        'recipient_author_id',
        'order_status_id',
        'shipment_port_name',
        'weight_type_id',
        'channel_id',
        'delivered_at',
        'delivery_date',
        'delivery_address',
        'delivery_term_id',
        'delivery_method_id',
        'delivery_postcode',
        'billing_address_id',
        'locality',
        'country_id',
        'payment_terms',
        'terms_of_sales',
        'external_reference_id',
        'po_reference',
        'currency_code',
        'send_to_users',
        'seller_reference',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'send_to_users'     => 'array',
        'payment_terms'     => 'text',
        'seller_reference'  => 'text',
        'terms_of_sales'    => 'text',
    ];

    /**
     * The attributes that will hidden.
     * @var array
     */
    protected $hidden = [
//        'created_at',
//        'updated_at'
    ];

    /**
     * @param $value
     * @throws \Exception
     */
    public function setDeliveryDateAttribute($value) {
        if (empty($value)) {
            return null;
        }

        // Accept 2 date formats
        // day-month-year
        // month-year
        $date = \DateTime::createFromFormat('Y-m', $value);
        if (!$date) {
            $date = \DateTime::createFromFormat('Y-m-d', $value);

            if (!$date) {
                throw new \Exception('the delivery_date is invalid format date!');
            }

            $value = $date->format('Y-m-d');
        }
        else {
            $value = $date->format('Y-m-00');
        }
        $this->attributes['delivery_date'] = $value;
    }

    /**
     * @param $value
     * @return string
     * @throws \Exception
     */
    public function getDeliveryDateAttribute($value) {
        if (empty($value)) {
            return null;
        }
        $date = \DateTime::createFromFormat('Y-m-00', $value);
        if (!$date) {
            $date = \DateTime::createFromFormat('Y-m-d', $value);

            if (!$date) {
                throw new \Exception('the delivery_date is invalid format date!');
            }

            $value = $date->format('Y-m-d');
        }
        else {
            $value = $date->format('Y-m');
        }
        return $value;
    }

    /**
     * all items of the order
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    /**
     * state of the order
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function deliveryTerms()
    {
        return $this->hasOne( Term::class, 'id', 'delivery_term_id');
    }

    public function deliveryMethod()
    {
        return $this->hasOne(Shipping::class,'id','delivery_method_id');
    }

    /**
     * Set status by status label
     * @param $label
     */
    public function setStatus($label)
    {
        $status = OrderStatus::query()
            ->where('label', '=', $label)->first();
        if (isset($status)) {
            $this->setAttribute('order_status_id', $status->id);
        }
    }

    /**
     * Get the group that owns the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(OrderGroup::class, 'order_group_id');
    }

    /**
     * Get all attachments owns the order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments() {
        return $this->hasMany(OrderAttachment::class, 'order_id');
    }

    /**
     * Calculate order totals summary
     * @return $this
     */
    public function calculateTotals() {
        $items = $this->items()->get(['unit_total', 'quantity']);
        $itemTotal = 0;
        $itemTotalQty = 0;
        foreach ($items as $item) {
            $itemTotal += $item->unit_total;
            $itemTotalQty += $item->quantity;
        }
        $this->items_total = count($items);
        $this->items_total_qty = $itemTotalQty;
        $this->total = $itemTotal;

        // We are using double(10,2) to store total.
        if ($this->total > 99999999.99) {
            throw new BadHttpRequestException('Total price of order exceeds 99999999.99, too big to store in database.');
        }

        return $this;
    }

    public function generateIdReadable($ref) {
        $clientId = ClientId::orgId($this->org_id)->first();

        $exists = true;
        while ($exists) {
            $id = $clientId->atomicBump();
            $exists = Orders::where('id_readable', $ref . '-' . $id)->exists();
        }

        $this->setAttribute('id_readable', $ref . '-' . $id);
    }

    /**
     * Prepare data for create new a order
     * @param array $attributes
     * @return array
     * @throws \Exception
     */
    public static function fromCreateData(array $attributes = []) {
        // Calculate totals and Create item instances
        $items = [];
        $itemTotal = 0;
        $itemTotalQty = 0;

        if (isset($attributes['items'])) {
            foreach ($attributes['items'] as $item) {
                $item = OrderItem::fromCreateData($item);
                $itemTotal += $item['unit_total'];
                $itemTotalQty += $item['quantity'];
                array_push($items, $item);
            }
        }

        if (isset($attributes['attachments'])) {
            $attachments = [];
            foreach ($attributes['attachments'] as $id) {
                $attachments[] = OrderAttachment::fromCreateData($id);
            }
            $attributes['attachments'] = $attachments;
        }

        $data = array_merge($attributes, [
            'uuid'              => (string)Uuid::uuid4(),
            'order_status_id'   => OrderStatus::getByLabel($attributes['state'])->id,
            'items'             => $items,
            'items_total'       => count($items),
            'items_total_qty'   => $itemTotalQty,
            'total'             => $itemTotal
        ]);
        return $data;
    }

    /**
     * Prepare data for update order
     * @param array $data
     * @return array
     */
    public static function fromUpdateData(array $data = []) {
        $attributes = $data['attributes'];

        if (isset($attributes['attachments'])) {
            $attachments = [];
            foreach ($attributes['attachments'] as $item) {
                $attachments[] = OrderAttachment::fromUpdateData($item);
            }

            $attributes = array_merge($attributes, [
                'attachments' => $attachments
            ]);
        }

        if (isset($attributes['state'])) {
            $attributes['order_status_id'] = OrderStatus::getByLabel($attributes['state'])->id;
            unset($attributes['state']);
        }

        if (isset($attributes['postCode'])) {
            $attributes['delivery_postcode'] = $attributes['postCode'];
            unset($attributes['postCode']);
        }

        if (isset($attributes['deliveryMethod'])) {
            $attributes['delivery_method_id'] = $attributes['deliveryMethod']['id'];
            unset($attributes['deliveryMethod']);
        }

        if (isset($attributes['deliveryTerm'])) {
            $attributes['delivery_term_id'] = $attributes['deliveryTerm']['id'];
            unset($attributes['deliveryTerm']);
        }

        if (isset($attributes['country'])) {
            $attributes['country_id'] = $attributes['country']['id'];
            unset($attributes['country']);
        }
        return Convention::snakeConvention($attributes);
    }

    public static function  generatePXCReferenceID($orgId, $orgName) {

        $clientId = ClientId::orgId($orgId)->first();
        if ($clientId) {
            return $clientId->ref;
        }

        // First 3 letters of the seller's org name
        $name = str_replace(' ', '', $orgName);
        $refId = strtoupper(substr($name, 0, 3));

        $order = Orders::query()
            ->where('id_readable',  'like', "$refId-%")
            ->first();

        if (isset($order)) {
            if ($order->org_id !== $orgId) {
                // Verify the first 3 letters are unique,
                // if not, then always apply then first letter of their org name and then for the remaining 2 characters,
                // random selection until unique 3 letter
                $charOrgName = $refId[0];

                $gotIt = false;
                while (!$gotIt) {
                    $refId = $charOrgName.Helpers::randomChars(2);
                    $check = Orders::query()
                        ->where('id_readable',  'like', "$refId-%")
                        ->first();
                    if (!$check) {
                        $gotIt = true;
                    }
                }
            }
        }

        $clientId = new ClientId();
        $clientId->org_uuid = $orgId;
        $clientId->ref = $refId;
        $clientId->save();

        return $refId;
    }
}
