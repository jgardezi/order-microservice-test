<?php

namespace App\Entities;

/**
 * Class Term.
 *
 * @package namespace App\Entities;
 */
class Term extends OrdersModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that are cast as Carbon instances.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at'];

    /**
     * The name attribute with the respecitve code.
     *
     * @param $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return $value. ' (' . $this->code . ')';
    }

    /**
     * @return array
     */
    public function transform()
    {
        return [
            'id' => (int) $this->id,
            'text' => $this->name
        ];
    }
}
