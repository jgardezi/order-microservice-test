<?php

namespace App\Entities;

use App\Entities\Traits\Updatable;
use Illuminate\Support\Facades\Request;

/**
 * Class OrderAttachment.
 *
 * @package namespace App\Entities;
 */
class OrderAttachment extends OrdersModel
{
    use Updatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_attachments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'attachment_id',
    ];

    protected $updatable = [
        'attachment_id',
    ];

    /**
     * The attributes that will hidden.
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Parse create data to model attributes
     * @param string $id
     * @return array
     */
    public static function fromCreateData($id) {
        $user = Request::user();
        return [
            'user_id'       => $user->id,
            'attachment_id' => $id
        ];
    }

    /**
     * Parse create data to model attributes
     * @param array $data
     * @return mixed
     */
    public static function fromUpdateData(array $data = []) {
        $user = Request::user();
        $attributes = $data['attributes'];
        return [
            'user_id'       => $user->id,
            'attachment_id' => $attributes['attachmentId'],
        ];
    }
}
