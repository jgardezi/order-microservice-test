<?php

namespace App\Entities;

/**
 * Class Shipping.
 *
 * @package namespace App\Entities;
 */
class Shipping extends OrdersModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shippings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that are cast as Carbon instances.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at'];

    /**
     * @return array
     */
    public function transform()
    {
        return [
            'id' => (int) $this->id,
            'text' => $this->name
        ];
    }
}
