<?php

namespace App\Entities\Traits;

trait CustomCasts {
    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if ($this->hasCast($key)) {
            $value = $this->castSetAttribute($key, $value);
        }
        return parent::setAttribute($key, $value);
    }

    protected function castSetAttribute($key, $value) {
        switch ($this->getCastType($key)) {
            case 'text':
                return $this->toText($value);
            default:
                return $value;
        }
    }


    protected function castAttribute($key, $value) {
        switch ($this->getCastType($key)) {
            case 'text':
                return $this->fromText($value);
            default:
                return parent::castAttribute($key, $value);
        }
    }

    protected function toText($value) {
        $value = trim(preg_replace('/\s+/', ' ', $value));
        return $value;
    }

    protected function fromText($value) {
        return $value;
    }
}