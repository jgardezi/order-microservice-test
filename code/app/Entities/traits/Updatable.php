<?php

namespace App\Entities\Traits;

trait Updatable {
    public function update(array  $attributes = [], array $options = []) {
        foreach ( $attributes as $key => $value) {
            if (!$this->isUpdatable($key)) {
                unset( $attributes[$key]);
            }
        }
        return $this->fill($attributes);
    }

    public function getUpdatable() {
        return isset($this->updatable) ? $this->updatable : [];
    }

    public function isUpdatable($key) {
        return in_array($key, $this->getUpdatable());
    }
}