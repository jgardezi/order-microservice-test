<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spiritix\LadaCache\Database\LadaCacheTrait;

/**
 * Class OrdersModel.
 * Base for all models in orders micro service.
 *
 * @package namespace App\Entities;
 */
class OrdersModel extends Model implements Transformable
{
    use TransformableTrait;
    use LadaCacheTrait;
}