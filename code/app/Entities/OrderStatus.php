<?php

namespace App\Entities;

/**
 * Class OrderStatus.
 *
 * @package namespace App\Entities;
 */
class OrderStatus extends OrdersModel
{
    const DRAFT = 'draft';
    const OFFER = 'offer';
    const PENDING = 'pending';
    const CONFIRMED = 'confirmed';
    const CLOSED = 'closed';
    const CANCELLED = 'cancelled';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'label', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public static function draft() {
        return self::query()
            ->where('label', '=', self::DRAFT)->first()->id;
    }

    public static function offer() {
        return self::query()
            ->where('label', '=', self::OFFER)->first()->id;
    }

    public static function getByLabel($label) {
        return self::query()
            ->where('label', '=', $label)->first();
    }

    /**
     * Get orders status by label.
     *
     * @param array $labels
     * @return array Order status ids
     */
    public static function getOrderStatusIdsByLabels(array $labels)
    {
        return self::query()->whereIn('label', $labels)->pluck('id')->toArray();
    }

    public static function getOrderStatusLabelById($id)
    {
        return self::query()->find($id)->label;
    }
}
