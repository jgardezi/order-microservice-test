<?php

namespace App\Jobs;

use App\Events\AssignChannelToOrderEvent;
use App\Events\OrganisationGeneralEvent;
use App\Presenters\OrderPresenter;
use App\Gateways\UserGateway;

class OrderCreatedJob extends Job
{
    private $order;
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $presenter = app(OrderPresenter::class);
        $userGateway = app(UserGateway::class);

        $order = $this->order;
        $group = $order->group;
        event(new AssignChannelToOrderEvent($group));

        // If not cached already - grab both at once.
        $userGateway->findByIds([
            $order->author_id,
            $order->recipient_author_id
        ]);

        // TODO: this is a hack for letting counter party see the created order
        // we should improve the logic and performance
        $payload = $presenter->present($order);
        $payload["data"]["id"] = $order->uuid;

        $author = $userGateway->findById($order->author_id);
        $authorOrganisationId = $author['organisation']['id'];

        $recipientAuthor = $userGateway->findById($order->recipient_author_id);
        $recipientAuthorOrganisationId = $recipientAuthor['organisation']['id'];

        event(new OrganisationGeneralEvent($authorOrganisationId, $payload["data"], OrderCreated::class));
        if($recipientAuthorOrganisationId && $recipientAuthorOrganisationId !== "") {
            event(new OrganisationGeneralEvent($recipientAuthorOrganisationId, $payload["data"], OrderCreated::class));
        }
    }
}
