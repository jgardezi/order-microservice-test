<?php

namespace App\Transformers;

use App\Entities\Orders;
use App\Entities\OrderStatus;
use App\Gateways\AttachmentGateway;
use App\Gateways\StockGateway;
use App\Gateways\UserGateway;
use App\Presenters\OrderAttachmentPresenter;
use App\Presenters\OrderItemPresenter;
use App\Repositories\OrderItemRepository;
use App\Supports\Convention;
use App\Supports\IncludesParser;
use App\Supports\Helpers;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    protected $item;

    protected $stockGateway;

    protected $userGateway;

    protected $attachmentGateway;

    protected $stockNeeded;

    protected $attachmentsNeeded;

    public function __construct(
        OrderItemRepository $item,
        StockGateway $stockGateway,
        UserGateway $userGateway,
        AttachmentGateway $attachmentGateway,
        IncludesParser $excluded
    )
    {
        $this->item = $item;
        $this->stockGateway = $stockGateway;
        $this->userGateway = $userGateway;
        $this->attachmentGateway = $attachmentGateway;
        $this->stockNeeded = $excluded->isIncluded('items');
        $this->attachmentsNeeded = $excluded->isIncluded('attachments');
    }

    public function transform(Orders $order)
    {
        $attributes = $order->toArray();
        $attributes['state'] = $order->status->label;

        $author = $this->userGateway->findById($order->author_id);
        $author = [
            'id'        => $author['id'],
            'firstName' => $author['attributes']['firstName'],
            'lastName'  => $author['attributes']['lastName'],
            'avatar'    => $author['attributes']['avatar']['path'],
            'orgName'   => $author['attributes']['organisation']['attributes']['name'],
        ];

        // Draft order might do not have recipient.
        if (isset($order->recipient_author_id)) {
            $recipientAuthor = $this->userGateway->findById($order->recipient_author_id);
            $attributes['recipientAuthor'] = [
                'id'        => $recipientAuthor['id'],
                'firstName' => $recipientAuthor['attributes']['firstName'],
                'lastName'  => $recipientAuthor['attributes']['lastName'],
                'avatar'    => $recipientAuthor['attributes']['avatar']['path'],
                'orgName'   => $recipientAuthor['attributes']['organisation']['attributes']['name'],
            ];
        }
        else if ($attributes['state'] === OrderStatus::DRAFT) {
            $attributes['recipientAuthors'] = [];
            if (is_array($order->send_to_users)) {
                foreach ($order->send_to_users as $id) {
                    $recipientAuthor = $this->userGateway->findById($id);
                    $attributes['recipientAuthors'][] = [
                        'id'        => $recipientAuthor['id'],
                        'firstName' => $recipientAuthor['attributes']['firstName'],
                        'lastName'  => $recipientAuthor['attributes']['lastName'],
                        'avatar'    => $recipientAuthor['attributes']['avatar']['path'],
                        'orgName'   => $recipientAuthor['attributes']['organisation']['attributes']['name'],
                    ];
                }
            }
        }

        $items = [];
        if ($this->stockNeeded && $order->items()->count() > 0) {
            $items = (new OrderItemPresenter())->present($order->items)['data'];
        }


        Helpers::unsetArray($attributes, [
            'author_id',
            'recipient_author_id',
            'delivery_method_id',
            'delivery_term_id',
            'country_id',
            'order_status_id',
            'delivery_postcode',
            'group',
            'send_to_users',
            'status'
        ]);

        $attributes = array_merge(Convention::camelConvention($attributes), [
            'note'      => $order->group->note,
            'subject'   => $order->group->subject,
            'postCode'  => $order->delivery_postcode,
            'items'     => $items,
            'author'    => $author,
            'deliveryMethod'    => [
                'id'    => $order->delivery_method_id,
                'name'  => 'string' //TODO:
            ],
            'deliveryTerm' => [
                'id'            => $order->delivery_term_id,
                'description'   => 'string' //TODO:
            ],
            'country' => [
                'id'    => $order->country_id,
                'code'  => 'string' //TODO:
            ],
        ]);

        if($this->attachmentsNeeded) {
            $attributes['attachments'] = $this->attachmentGateway->findById($order->uuid)->attachments;
        } else {
            $attributes['hasAttachments'] = $this->attachmentGateway->findById($order->uuid)->hasAttachments;
        }
        
        $attributes['createdAt'] = $order->created_at->toIso8601String();
        $attributes['updatedAt'] = $order->updated_at->toIso8601String();

        return [
            'id'    => $order->id,
            'type'  => 'order',
            'attributes' => $attributes,
            'links' => [
                'self' => url('/orders/' . $order->id ),
            ]
        ];
    }

    public function transformUpdates(Orders $order, array $updates)
    {
        $result = [
            'id' => $order->uuid,
            'type' => 'order',
        ];

        if (isset($updates['items'])) {
            $xfmr = app(OrderItemTransformer::class);
            foreach ($updates['items'] as &$item) {
                if (isset($item['deleted'])) {
                    continue;
                }
                $item = $xfmr->transform($item);// TODO do we want a lighter transform for pusher?
            }
        }

        $attributes = Convention::camelConvention($updates);
        $attributes['updatedAt'] = Carbon::now()->toIso8601String();

        if (isset($updates['order_status_id'])) {
            $attributes['state'] = OrderStatus::getOrderStatusLabelById($updates['order_status_id']);
        }

        if(!empty($order->channelId)) {
            $attributes['channelId'] = $order->channelId;
        }

        $result['attributes'] = $attributes;

        return $result;
    }
}