<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/2/19
 * Time: 11:09 AM
 */
namespace App\Transformers;

use App\Entities\OrderItem;
use App\Gateways\StockGateway;
use App\Supports\Convention;
use League\Fractal\TransformerAbstract;

class OrderItemTransformer extends TransformerAbstract
{
    /**
     * @var StockGateway
     */
    protected $stockGateway;

    public function __construct()
    {
        $this->stockGateway = app(StockGateway::class);
    }

    public function transform(OrderItem $item)
    {
        $attributes = $item->toArray();
        $attributes = Convention::camelConvention($attributes);
        unset($attributes['id']);
        unset($attributes['locationCode']);

        $attributes['createdAt'] = $item->created_at->toIso8601String();
        $attributes['updatedAt'] = $item->updated_at->toIso8601String();

        $stock = $this->stockGateway
            ->findByLocation(
                $item->product_id,
                $item->location_code
            );

        if ($stock) {
            $attributes['skuId'] = $stock->sku;
            $attributes['chilledStatus'] = $stock->chilledStatus;
            $attributes['description'] = $stock->description;
            $attributes['stockId'] = $stock->stockId;
            $attributes['location'] = [
                'code' => $stock->facility['code'],
                'name' => $stock->facility['name'],
            ];
        }
        else {
            $attributes['skuId'] = null;
            $attributes['chilledStatus'] = null;
            $attributes['description'] = null;
            $attributes['stockId'] = null;
            $attributes['location'] = [
                'code' => null,
                'name' => null,
            ];
        }

        return [
            'id' => $item->id,
            'type' => 'order_item',
            'attributes' => $attributes,
        ];
    }

}
