<?php
namespace App\Transformers;

use App\Entities\OrderAttachment;
use League\Fractal\TransformerAbstract;

class OrderAttachmentTransformer extends TransformerAbstract
{
    public function transform(OrderAttachment $item)
    {
        $attributes = $item->toArray();
        return [
            'id' => $attributes['id'],
            'type' => 'order_attachment',
            'attributes' => [
                'userId' => $attributes['user_id'],
                'attachmentId' => $attributes['attachment_id'],
                'createdAt' => $item->created_at->toIso8601String(),
                'updatedAt' => $item->updated_at->toIso8601String(),
            ],
        ];
    }
}
