<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/2/19
 * Time: 11:09 AM
 */
namespace App\Transformers;

use App\Entities\OrderGroup;
use App\Presenters\OrderPresenter;
use League\Fractal\TransformerAbstract;

class OrderGroupTransformer extends TransformerAbstract
{
    /**
     * @var OrderPresenter
     */
    protected $orderPresenter;

    public function __construct(OrderPresenter $orderPresenter)
    {
        $this->orderPresenter = $orderPresenter;
    }

    public function transform(OrderGroup $group) {
        $res = $this->orderPresenter->present($group->orders);

        if (sizeof($res['data']) === 1) {
            return array_get($res['data'], 0, []);
        }

        $recipients = [];
        foreach ($res['data'] as $order) {
            if (isset($order['attributes']['recipientAuthor'])) {
                $recipients[] = $order['attributes']['recipientAuthor'];
            }
        }

        return [
            'id'    => $group->id,
            'type'  => 'orderGroup',
            'attributes' => [
                'subject'           => $group->subject,
                'note'              => $group->note,
                'totalAmount'       => $group->total_amount,
                'orderTotalQty'     => $group->order_total_qty,
                'recipientAuthors'  => $recipients,
                'orders'            => $res['data'],
                'createdAt'         => $group->created_at->toIso8601String(),
                'updatedAt'         => $group->updated_at->toIso8601String(),
            ],
        ];
    }

}