<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/24/19
 * Time: 4:29 PM
 */

namespace App\Constants;


class UserRole
{
    const SUPER = 'super';
    const ADMINISTRATOR = 'administrator';
    const OWNER = 'owner';
    const EMPLOYEE = 'employee';
    const CUSTOM_MODULE = 'custom_module';
    const MARKETPLACE_MODULE = 'marketplace_module';
    const MANAGER = 'manager';
    const SUPERVISOR = 'supervisor';
}