<?php


namespace App\Supports;


use App\Gateways\AttachmentGateway;
use App\Gateways\StockGateway;
use App\Gateways\UserGateway;

class ExternalData
{
    private $userGateway;
    private $stockGateway;
    private $attachmentGateway;

    function __construct(UserGateway $userGateway, StockGateway $stockGateway, AttachmentGateway $attachmentGateway)
    {
        $this->userGateway = $userGateway;
        $this->stockGateway = $stockGateway;
        $this->attachmentGateway = $attachmentGateway;
    }

    /**
     * Traverses a list of groups extracting user and stock info to call the
     * respective APIs in bulk, causing the caches to fill with one API call
     * each.
     * @param $groups
     */
    public function loadExternalData($groups)
    {
        if (count($groups) === 0) {
            return;
        }

        $stockNeeded = app(IncludesParser::class)->isIncluded('items');

        // Collect all users and stock to call APIs once each.
        $userIds=[];
        $orderIds=[];
        $stock=[];
        $organisation_id = $groups->first()->orders->first()->org_id;
        foreach ($groups as $group) {
            foreach ($group->orders as $order) {

                //Users
                $userIds[] = $order->author_id;
                $userIds[] = $order->recipient_author_id;
                if ($order->send_to_users) {
                    foreach ($order->send_to_users as $recipient) {
                        $userIds[] = $recipient;
                    }
                }

                // Stock
                if ($stockNeeded) {
                    foreach ($order->items as $item ) {
                        if ($item->production_dates && count($item->production_dates)) {
                            foreach ($item->production_dates as $date) {
                                $key = $item->product_id . $item->location_code . $date;
                                $stock[$key] = [
                                    'productUuid'       => $item->product_id,
                                    'facilityCode'      => $item->location_code,
                                    'productionDate'    => $date,
                                ];
                            }
                        } else {
                            $key = $item->product_id . $item->location_code;
                            $stock[$key] = [
                                'productUuid'       => $item->product_id,
                                'facilityCode'      => $item->location_code,
                            ];
                        }
                    }
                }

                // Attachments
                $orderIds[] = $order->uuid;
            }
        }

        $userIds = array_unique($userIds);
        $stockRequest = [];
        foreach ($stock as $item) {
            $stockRequest[] = $item;
        }

        // Get the remote data. The returned data is cached in the gateway classes.
        $this->userGateway->findByIds($userIds);
        $this->attachmentGateway->findByIds($orderIds);
        if (!empty($stockRequest)) {
            $this->stockGateway->setOrg($organisation_id);
            $this->stockGateway->filter($stockRequest);
        }
    }
}