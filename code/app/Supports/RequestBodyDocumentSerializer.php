<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/29/19
 * Time: 6:06 PM
 */

namespace App\Supports;

use Illuminate\Support\Facades\Log;
use Swis\JsonApi\Client\Interfaces\ItemDocumentInterface;

class RequestBodyDocumentSerializer extends \Swis\JsonApi\Client\ItemDocumentSerializer
{
    /**
     * @param ItemDocumentInterface $itemDocument
     * @return string
     * @throws \Exception
     */
    public function serialize(ItemDocumentInterface $itemDocument)
    {
        if (empty($itemDocument->getData())) {
            throw new \Exception('Data is required!');
        }
        $document = $itemDocument->getData()->toJsonApiArray();
        $json = json_encode(array_get($document, 'attributes', [])['data']);
        return $json;
    }

}