<?php

namespace App\Supports;


class Convention {
    /**
     * @param array $attributes
     * @return array
     */
    public static function camelConvention(array $attributes = []) {
        $newArray = [];
        foreach ($attributes as $attr => $value) {
            $newArray[self::camelize($attr)] = $value;
        }
        return array_reverse($newArray);
    }

    /**
     * @param array $attributes
     * @return array
     */
    public static function snakeConvention(array $attributes = []) {
        $newArray = [];
        foreach ($attributes as $attr => $value) {
            $newArray[self::decamelize($attr)] = $value;
        }
        return array_reverse($newArray);
    }

    /**
     * @param $string
     * @param bool $capitalizeFirstCharacter
     * @return mixed|string
     */
    public static function camelize($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
        return $str;
    }

    /**
     * @param $string
     * @return string
     */
    public static function decamelize($string) {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
    }
}