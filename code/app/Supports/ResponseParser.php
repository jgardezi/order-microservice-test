<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/29/19
 * Time: 6:06 PM
 */

namespace App\Supports;

use Art4\JsonApiClient\Utils\Helper;
use Art4\JsonApiClient\Utils\Manager as Art4JsonApiClientManager;
use Swis\JsonApi\Client\Interfaces\DocumentInterface;
use Swis\JsonApi\Client\JsonApi\Hydrator;
use Swis\JsonApi\Client\JsonApi\Parser;

class ResponseParser extends Parser
{
    public function __construct(Art4JsonApiClientManager $manager, Hydrator $hydrator, ResponseErrorsParser $errorsParser)
    {
        parent::__construct($manager, $hydrator, $errorsParser);
    }

    public function deserialize(string $json): DocumentInterface
    {
        $data = Helper::decodeJson($json);
        if (isset($data->errors) && is_string($data->errors)) {
            $data->errors = [
                [
                    "detail" => $data->errors
                ]
            ];
            $json = json_encode($data);
        }
        return parent::deserialize($json);
    }
}