<?php


namespace App\Supports;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Application;

/**
 * Class IncludesParser
 * Singleton that parses the 'include' and 'exclude' GET parameters on first use of
 * isIncluded or isNotIncluded.
 * Controllers should call addDefaultInclude in action methods or their constructor.
 *
 * @package App\Supports
 */
class IncludesParser
{
    private $includeByDefault;
    private $included;
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->included = null;
    }

    public function parse()
    {
        $this->included = $this->includeByDefault;

        if ($this->request->has('include')) {
            $includes = explode(',', $this->request->input('include'));
            foreach ($includes as $include) {
                $this->included[$include] = true;
            }
        }

        if ($this->request->has('exclude')) {
            $excludes = explode(',', $this->request->input('exclude'));
            foreach ($excludes as $exclude) {
                unset($this->included[$exclude]);
            }
        }
    }

    public function addDefaultInclude($include)
    {
        $this->includeByDefault[$include] = true;
        $this->included = null;
    }

    public function removeDefaultInclude($include)
    {
        unset($this->includeByDefault[$include]);
        $this->included = null;
    }

    public function isIncluded($include)
    {
        if(is_null($this->included)) {
            $this->parse();
        }
        if (isset($this->included[$include])) {
            return true;
        }

        return false;
    }

    public function isNotIncluded($include)
    {
        if(is_null($this->included)) {
            $this->parse();
        }
        if (isset($this->included[$include])) {
            return false;
        }

        return true;
    }
}