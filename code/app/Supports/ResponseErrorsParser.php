<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/29/19
 * Time: 6:06 PM
 */

namespace App\Supports;

use Art4\JsonApiClient\ErrorCollection;
use Swis\JsonApi\Client\JsonApi\ErrorsParser;

class ResponseErrorsParser extends ErrorsParser
{
    public function parse(ErrorCollection $errorCollection)
    {
        return parent::parse($errorCollection);
    }
}