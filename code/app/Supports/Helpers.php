<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 1/11/19
 * Time: 12:55 PM
 */

namespace App\Supports;


class Helpers
{
    public static function unsetArray(&$array, $keys) {
        foreach ($keys as $key) {
            if (isset($array[$key])) {
                unset($array[$key]);
            }
        }
    }

    public static function findIndex($value, $array, callable $callback) {
        foreach ($array as $key => $item) {
            if ($callback($item) == $value) {
                return $key;
            }
        }
        return -1;
    }

    public static function randomChars($len = 1) {
        return substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $len);
    }

    public static function debugQuery($query)
    {
        $sql = $query->toSql();
        $bindings = $query->getBindings();
        $query = str_replace(array('?'), array('\'%s\''), $sql);
        $query = vsprintf($query, $bindings);

        \Log::debug(wordwrap($query));
    }
}