<?php

namespace App\Listeners;

use App\Entities\OrderStatus;
use App\Events\OrderCreated;
use App\Events\OrderStatusChanged;
use App\Events\OrderUpdated;
use App\Events\OrganisationGeneralEvent;
use App\Gateways\ProductGateway;
use App\Gateways\UserGateway;
use App\Presenters\OrderPresenter;
use App\Services\EmailQueueClientInterface;
use App\Transformers\OrderTransformer;
use Carbon\Carbon;

class OrderEventSubscriber
{
    protected $productGateway;
    protected $userGateway;

    public function __construct(ProductGateway $gateway, UserGateway $userGateway)
    {
        $this->productGateway = $gateway;
        $this->userGateway = $userGateway;
    }

    public function onCreated (OrderCreated $event) {
        if ($event->order->status->label === OrderStatus::DRAFT) return;

        $emailClient = app(EmailQueueClientInterface::class);

        $this->productGateway->setOrg($event->order->org_id);
        $this->productGateway->loadOrderProducts($event->order);

        $items = [];
        foreach ($event->order->items as $orderItem) {
            $product = $this->productGateway->findById($orderItem->product_id);
            $items[] = [
                "id"          => $orderItem->id,
                "productId"   => $orderItem->product_id,
                "sku"         => $product->sku,
                "description" => $product->description,
                "chilledStatus" => $product->chilledStatus,
                "quantity"    => $orderItem->quantity,
                "price"       => $orderItem->unit_price
            ];
        }

        $type = 'order';
        if ($event->order->status->label == OrderStatus::OFFER) {
            $type = "offer";
        }

        $emailClient->send(
            'order.' . $event->order->uuid . '.' . Carbon::now()->toIso8601String(),
            'order.' . $event->order->uuid,
            [
                "senderID" => $event->order->author_id,
                "receiverIDs" => [$event->order->recipient_author_id],
                "isSystemEmail" => true,
                "scope" => "order.new.$type",
                "data" => [
                    'id'        => $event->order->uuid,
                    "note"      => $event->order->group->note,
                    "subject"   => $event->order->group->subject,
                    'authorId'  => $event->order->author_id,
                    "items"     => $items,
                    "channelId" => $event->order->channel_id,
                ]
            ]
        );
    }

    /**
     * @param OrderUpdated $event
     */
    public function onUpdated (OrderUpdated $event) {
        if ($event->order->status->label === OrderStatus::DRAFT) return;

        // Send email only if status was not updated.
        if( !isset($event->updates['order_status_id'])) {
            $emailClient = app(EmailQueueClientInterface::class);

            $receiverIds = $this->whoGetsTheEmail($event->order, $event->whoChanged);

            $emailClient->send(
                'order.' . $event->order->uuid . '.' . Carbon::now()->toIso8601String(),
                'order.' . $event->order->uuid,
                [
                    "senderID" => $event->whoChanged,
                    "receiverIDs" => $receiverIds,
                    "isSystemEmail" => true,
                    "scope" => "order.updated",
                    "data" => [
                        'id'        => $event->order->uuid,
                        "note"      => $event->order->group->note,
                        "subject"   => $event->order->group->subject,
                        'authorId'  => $event->order->author_id,
                        "channelId" => $event->order->channel_id,
                        'state'     => $event->order->status->label,
                    ]
                ]
            );
        }

        // Do organisation based summary event for now to save some request time
        $payload = (app(OrderTransformer::class))
            ->transformUpdates($event->order, $event->updates);

        // Cause both to be retrieved at once (and cached).
        $this->userGateway->findByIds([
            $event->order->author_id,
            $event->order->recipient_author_id,
        ]);

        $author = $this->userGateway->findById($event->order->author_id);
        $authorOrganisationId =$author['organisation']['id'];

        $recipientAuthor = $this->userGateway->findById($event->order->recipient_author_id);
        $recipientAuthorOrganisationId = $recipientAuthor['organisation']['id'];

        event(new OrganisationGeneralEvent($authorOrganisationId, $payload, OrderUpdated::class));
        if($recipientAuthorOrganisationId && $recipientAuthorOrganisationId !== "") {
            event(new OrganisationGeneralEvent($recipientAuthorOrganisationId, $payload, OrderUpdated::class));
        }
    }

    /**
     * @param OrderStatusChanged $event
     */
    public function onStatusChanged(OrderStatusChanged $event) {
        if ($event->oldStatus === OrderStatus::DRAFT) return;
        // Should a pending order go back to offer, don't send.
        if ($event->newStatus === OrderStatus::OFFER) return;

        $emailClient = app(EmailQueueClientInterface::class);

        $receiverIds = $this->whoGetsTheEmail($event->order, $event->whoChanged);

        $emailClient->send(
            'order.' . $event->order->uuid . '.' . Carbon::now()->toIso8601String(),
            'order.' . $event->order->uuid,
            [
                "senderID" => $event->whoChanged,
                "receiverIDs" => $receiverIds,
                "isSystemEmail" => true,
                "scope" => "order.status.changed",
                "data" => [
                    'id'        => $event->order->uuid,
                    "note"      => $event->order->group->note,
                    "subject"   => $event->order->group->subject,
                    'authorId'  => $event->order->author_id,
                    'recipientAuthorId'  => $event->order->recipient_author_id,
                    "oldStatus" => $event->oldStatus,
                    "newStatus" => $event->newStatus,
                    "channelId" => $event->order->channel_id,
                ]
            ]
        );
    }

    function whoGetsTheEmail($order, $whoChanged)
    {
        // Author made the change.
        if ($order->author_id === $whoChanged) {
            return [ $order->recipient_author_id ];
        }

        // Recipient (buyer) made the change.
        if ($order->recipient_author_id === $whoChanged) {
            return [ $order->author_id ];
        }

        // Wasn't either author or recipient. Send an email to both.
        return [$order->author_id, $order->recipient_author_id];
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            OrderCreated::class,
            OrderEventSubscriber::class.'@onCreated'
        );
        $events->listen(
            OrderUpdated::class,
            OrderEventSubscriber::class.'@onUpdated'
        );

        $events->listen(
            OrderStatusChanged::class,
            OrderEventSubscriber::class.'@onStatusChanged'
        );
    }
}
