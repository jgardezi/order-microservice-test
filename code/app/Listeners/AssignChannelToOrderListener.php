<?php

namespace App\Listeners;

use App\Events\AssignChannelToOrderEvent;
use App\Gateways\ChannelGateway;
use App\Gateways\GroupGateway;
use App\Gateways\UserGateway;
use App\Supports\Helpers;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignChannelToOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignChannelToOrderEvent  $event
     * @return void
     */
    public function handle(AssignChannelToOrderEvent $event)
    {
        $userGateway = app(UserGateway::class);
        $orderGroups = [];
        $orderChannels = [];

        // 1. Get all recipient of the OrderGroup
        $recipientIds = [];
        foreach ($event->group->orders as $order) {
            $recipientIds[] = $order->author_id;
            $recipientIds[] = $order->recipient_author_id;
            if (isset($order->channel_id)) {
                $orderGroups[] = $order;
            }
            else {
                $orderChannels[] = $order;
            }
        }

        $hasOrderGroup = (sizeof($orderGroups) > 0);
        $hasOrderChannel = (sizeof($orderChannels) > 0);
        
        if ($hasOrderChannel || $hasOrderGroup) {
            $recipients = $userGateway->findByIds(array_unique($recipientIds));

            // Note: Create and assign channel ID
            if ($hasOrderChannel) {
                $channelGateway = app(ChannelGateway::class);

                // 2. Get all user's organisation ID.
                $userOrganisationIDs = [];
                foreach ($recipients as $recipient) {
                    $userOrganisationIDs[$recipient->getId()] = $recipient->organisation['id'];
                }

                // 3. Make channel data list and create channels for those orders.
                $channels = [];
                foreach ($orderChannels as $order) {
                    $authorOrgId = $userOrganisationIDs[$order->author_id];
                    $recipientOrgId = $userOrganisationIDs[$order->recipient_author_id];

                    $channels[] = [
                        "channelType"   => "external",
                        "authorId"      => $order->author_id,
                        "organisations" => [
                            $authorOrgId, $recipientOrgId
                        ],
                        "groups" => [
                            [
                                "authorId"  => $order->author_id,
                                "isPublic"  => false,
                                "groupType" => "direct",
                                "recipientUserIDs" => [
                                    // TODO remove this once channel service is using the one below.
                                    $order->recipient_author_id
                                ],
                                "recipientUserIds" => [
                                    $order->recipient_author_id
                                ]
                            ]
                        ]
                    ];
                }
                $channels = $channelGateway->create($channels);

                // 4. Mapping channels with orders and saving the channel ID for orders.
                foreach ($orderChannels as $order) {
                    $authorOrgId = $userOrganisationIDs[$order->author_id];
                    $recipientOrgId = $userOrganisationIDs[$order->recipient_author_id];

                    foreach ($channels as $channel) {
                        $organisations = $channel->organisations;
                        $authorIndex = Helpers::findIndex($authorOrgId, $organisations, function ($org) {
                            return $org->id;
                        });

                        $recipientIndex = Helpers::findIndex($recipientOrgId, $organisations, function ($org) {
                            return $org->id;
                        });

                        if ($authorIndex !== -1 && $recipientIndex !== -1) {
                            $order->update([
                                'channel_id' => $channel->getId()
                            ]);
                            $order->save();
                        }
                    }
                }
            }

            // Note: Order already have channel ID. just create groups direct message.
            if ($hasOrderGroup) {
                $groupGateway = app(GroupGateway::class);

                $groups = [];
                foreach ($orderGroups as $order) {
                    $groups[] = [
                        "channelId" => $order->channel_id,
                        "authorId"  => $order->author_id,
                        "isPublic"  => false,
                        "groupType" => "direct",
                        "users" => [
                            $order->recipient_author_id
                        ]
                    ];
                }
                $groupGateway->create($groups);
            }
        }

    }
}
