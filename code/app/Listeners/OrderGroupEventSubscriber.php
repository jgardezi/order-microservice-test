<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Events\OrderGroupCreated;
use \Event;

class OrderGroupEventSubscriber
{
    /**
     * @param OrderGroupCreated $event
     */
    public function onCreated (OrderGroupCreated $event) {
        foreach ($event->group->orders()->get() as $order) {
            Event::dispatch(new OrderCreated($order));
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            OrderGroupCreated::class,
            OrderGroupEventSubscriber::class.'@onCreated'
        );
    }
}
