<?php

namespace App\Listeners;

use App\Entities\Orders;
use App\Entities\OrderStatus;
use App\Events\AssignPXCReferenceIDToOrderEvent;
use App\Gateways\UserGateway;

class AssignPXCReferenceIDToOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignPXCReferenceIDToOrderEvent  $event
     * @return void
     */
    public function handle(AssignPXCReferenceIDToOrderEvent $event)
    {
        $userGateway = app(UserGateway::class);

        if (is_a($event->order, Orders::class)) {
            $user = $userGateway->findById($event->order->author_id);
        } else {
            $user = $userGateway->findById($event->order->orders->first()->author_id);
        }

        $refId = Orders::generatePXCReferenceID($user->organisation['id'], $user->organisation['attributes']['name']);

        if (is_a($event->order, Orders::class)) {
            if ($event->order->status->label !== OrderStatus::DRAFT && !$event->order->id_readable) {
                $event->order->generateIdReadable($refId);
                $event->order->save();
            }
            return;
        }

        foreach ($event->order->orders as $order) {
            if ($order->status->label !== OrderStatus::DRAFT && !$order->id_readable) {
                $order->generateIdReadable($refId);
                $order->save();
            }
        }
    }
}
