<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 1/28/19
 * Time: 10:15 AM
 */

namespace App\Gateways;

use PXC\JsonApi\Client\MachineRequestInterface;

class ChannelGateway extends RemoteGateway
{
    public $client;

    public function __construct(MachineRequestInterface $client)
    {
        $this->client = $client;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        parent::__construct();
    }

    /**
     * @param array $data
     * @return \Swis\JsonApi\Client\Interfaces\DocumentInterface
     */
    public function create(array $data) {
        $res = $this->client->post('channels', $this->makeRequestBody($data), false);
        return $this->handleResponse($res);
    }
}