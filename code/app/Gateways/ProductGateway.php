<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 2/17/19
 * Time: 3:23 PM
 */

namespace App\Gateways;


use App\Entities\Orders;
use Illuminate\Support\Collection;
use PXC\JsonApi\Client\MachineRequestInterface;

class ProductGateway extends RemoteGateway
{
    /**
     * @var MachineRequestInterface
     */
    private $client;

    /**
     * @var Collection
     */
    protected $data;

    protected $org_uuid;

    /**
     * ProductGateway constructor.
     * @param MachineRequestInterface $client
     */
    public function __construct(MachineRequestInterface $client)
    {
        $this->client = $client;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        parent::__construct();
    }

    public function setOrg($org_uuid)
    {
        $this->org_uuid = $org_uuid;
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function filter($ids = []) {
        $body = $this->makeRequestBody(['ids' => $ids]);
        $res = $this->client->post('products/filter?org_uuid=' . $this->org_uuid, $body,false);
        $this->cache($res);
        return $this->handleResponse($res);
    }

    public function loadOrderProducts(Orders $order) {
        if ($order->items()->count() > 0) {
            $items = [];
            foreach ($order->items as $item) {
                $items[] = $item->product_id;
            }
            if (sizeof($items) > 0) {
                $this->filter($items);
            }
        }
    }
}