<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 2/13/19
 * Time: 11:54 AM
 */

namespace App\Gateways;

use Illuminate\Support\Collection;
use Swis\JsonApi\Client\Item;
use Swis\JsonApi\Client\ItemDocument;

abstract class RemoteGateway
{
    /**
     * @var Collection
     */
    protected $data;

    public function __construct()
    {
        $this->data = new Collection();
    }

    public function makeRequestBody($data) {
        $body = new ItemDocument();
        $item = new Item([
            'data' => $data
        ]);
        $item->setType('data');
        $body->setData($item);
        return $body;
    }

    public function handleResponse($response) {
        return $response->getData();
    }

    /**
     * Cache data
     *
     * @param $response
     */
    public function cache($response) {
        $data = $response->getData();

        if (!$data) return;

        foreach ($data as $item) {
            $this->data->put($item->getId(), $item);
        }
    }

    /**
     * Find by collection key
     *
     * @param $id
     * @return mixed
     */
    public function findById($id) {
        $ret = $this->findByIds([$id]);
        if (empty($ret)) {
            return null;
        }

        return $ret->first();
    }

    /**
     * Find by collection keys
     *
     * @param $ids
     * @return mixed
     */
    public function findByIds($ids = []) {
        $query = $this->query();

        $result = [];
        foreach ($ids as $i => $id) {
            if (!$id) {
                unset($ids[$i]);
                continue;
            }
            if (!$query->has($id)) {
                $result[] = $id;
            }
        }

        if (sizeof($result) > 0) {
            $this->filter($result);
        }

        return $query->only($ids);
    }

    /**
     * @return Collection
     */
    public function query() {
        return $this->data;
    }
}