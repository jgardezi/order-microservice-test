<?php

namespace App\Gateways;

use App\Supports\IncludesParser;
use PXC\JsonApi\Client\MachineRequestInterface;

class AttachmentGateway extends RemoteGateway
{
    private $client;
    private $attachmentsNeeded;

    /**
     * UserGateway constructor.
     * @param MachineRequestInterface $client
     */
    public function __construct(MachineRequestInterface $client, IncludesParser $excluded)
    {
        $this->client = $client;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        $this->attachmentsNeeded = $excluded->isIncluded('attachments');
        parent::__construct();
    }

    /**
     * @param $id
     * @return \Swis\JsonApi\Client\Interfaces\DocumentInterface
     */
    public function get($id)
    {
        $attachments = $this->findById($id);
        return $attachments;
    }

    /**
     * @param array $data
     * @return \Swis\JsonApi\Client\Interfaces\DocumentInterface
     */
    public function filter($data = [])
    {
        $data = [
            'ids' => $data,
        ];
        if(!$this->attachmentsNeeded) {
            $data['fields'] = ['hasAttachments'];
        }
        $res = $this->client->post('orders/attachments/filter', $this->makeRequestBody($data), false);
        $this->cache($res);
        return $this->handleResponse($res);
    }

    /**
     * @param $organisationId
     * @return mixed
     */
    public function getOrganisationUsers($organisationId)
    {
        $res = $this->client->get('organisations/' . $organisationId . '/users', false);
        $this->cache($res);
        return $this->handleResponse($res);
    }
}