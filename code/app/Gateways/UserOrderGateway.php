<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 1/2/19
 * Time: 12:22 PM
 */

namespace App\Gateways;

use App\Criteria\TeamOrderCriteria;
use App\Criteria\UserOfferCriteria;
use App\Criteria\UserOrderCriteria;
use App\Repositories\OrderGroupRepository;
use App\Repositories\OrdersRepository;

class UserOrderGateway
{
    private $repository;
    private $data;
    private $org_id;
    private $errors;
    private $userGateway;
    private $group;

    public function __construct(OrdersRepository $order, OrderGroupRepository $group, UserGateway $userGateway)
    {
        $this->group = $group;
        $this->repository = $order;
        $this->org_id = 1; // TODO get via dependency injection.
        $this->userGateway = $userGateway;
    }

    public function get($id) {
        /** TODO */
    }

    public function filter($limit = 10) {
        $this->repository->pushCriteria(app(UserOrderCriteria::class));
        $orders = $this->repository->paginate($limit);
        return $orders;
    }

    public function offers($limit = 10) {
        $this->repository->pushCriteria(app(UserOfferCriteria::class));
        $orders = $this->repository->paginate($limit);
        return $orders;
    }

    public function store($data)
    {
        /** TODO */
    }

    public function update()
    {
        /** TODO */
    }

    public function delete()
    {
        /** TODO */
    }

    public function teamMembers($user) {
        $orgId = $user->org_id;
        $ids = $this->repository->findTeamMemberIds($orgId, [$user->id]);
        if (sizeof($ids) === 0) {
            return [
                "data" => []
            ];
        }
        $res = $this->userGateway->findByIds($ids);
        return $res;
    }

    public function teamOrders($limit = 10) {
        $this->group->pushCriteria(app(TeamOrderCriteria::class));
        $orders = $this->group->paginate($limit);
        return $orders;
    }
}