<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 1/2/19
 * Time: 12:22 PM
 */

namespace App\Gateways;

use PXC\JsonApi\Client\MachineRequestInterface;

class UserGateway extends RemoteGateway
{
    private $client;

    /**
     * UserGateway constructor.
     * @param MachineRequestInterface $client
     */
    public function __construct(MachineRequestInterface $client)
    {
        $this->client = $client;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        parent::__construct();
    }

    /**
     * @param $id
     * @return \Swis\JsonApi\Client\Interfaces\DocumentInterface
     */
    public function get($id) {
        $user = $this->findById($id);
        return $user;
    }

    /**
     * @param array $data
     * @return \Swis\JsonApi\Client\Interfaces\DocumentInterface
     */
    public function filter($data = []) {
        $data = [
            'ids' => $data
        ];
        $res = $this->client->post('users/filter', $this->makeRequestBody($data), false);
        $this->cache($res);
        return $this->handleResponse($res);
    }

    /**
     * @param $organisationId
     * @return mixed
     */
    public function getOrganisationUsers($organisationId) {
        $res = $this->client->get('organisations/' . $organisationId . '/users', false);
        $this->cache($res);
        return $this->handleResponse($res);
    }
}