<?php

namespace App\Gateways;

use App\Criteria\OfferCriteria;
use App\Criteria\OrderCriteria;
use App\Criteria\OrderGroupCriteria;
use App\Entities\OrderItem;
use App\Entities\OrderStatus;
use App\Repositories\OrderGroupRepository;
use App\Repositories\OrdersRepository;
use App\Services\EmailQueueClientInterface;
use App\Supports\Convention;
use App\Supports\Helpers;
use App\Events\OrderUpdated;
use App\Transformers\OrderTransformer;
use App\Transformers\OrderItemTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class OrderGateway
{
    private $repository;
    private $errors = [];
    private $group;
    private $emailQueueClient;
    private $stockGateway;
    private $productGateway;
    private $request;

    /**
     * OrderGateway constructor.
     * @param OrdersRepository $repository
     * @param OrderGroupRepository $group
     * @param EmailQueueClientInterface $emailQueueClient
     * @param StockGateway $stockGateway
     * @param ProductGateway $productGateway
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(
        OrdersRepository $repository,
        OrderGroupRepository $group,
        EmailQueueClientInterface $emailQueueClient,
        StockGateway $stockGateway,
        ProductGateway $productGateway,
        \Illuminate\Http\Request $request
    )
    {
        $this->repository = $repository;
        $this->group = $group;
        $this->emailQueueClient = $emailQueueClient;
        $this->stockGateway = $stockGateway;
        $this->productGateway = $productGateway;
        $this->request = $request;
    }

    /**
     * Get order by id
     * @param $id
     * @return mixed|\App\Entities\Orders
     */
    public function get($id) {
        $res = $this->repository
            ->with(['status', 'items', 'deliveryTerms', 'deliveryMethod'])
            ->findWhereFirst([ 'uuid' => $id ]);

        if (!isset($res)) {
            $this->addError([
                'status'   => 404,
                'detail'   => 'Order not found!'
            ]);
        }
        return $this->handleResult($res);
    }

    /**
     * Filter order by OrderCriteria
     * @param null $limit
     * @return mixed
     */
    public function filter($limit = null) {
        if ($this->request->has('channelId')) {
            $this->repository->pushCriteria(app(OrderCriteria::class));
            $orders = $this->repository->paginate($limit);

        } else {
            $this->group->pushCriteria(app(OrderGroupCriteria::class));
            $orders = $this->group->paginate($limit);
        }
        return $orders;
    }

    /**
     * Get Order offers
     * @param int $limit
     * @return mixed
     */
    public function offers($limit = 10) {
        $this->repository->pushCriteria(app(OfferCriteria::class));
        $orders = $this->repository->paginate($limit);
        return $orders;
    }

    /**
     * Store new order
     * @param array $data
     * @return mixed
     */
    public function store($data) {
        $isDraft = (
            $data['state'] === OrderStatus::DRAFT
        );

        if (isset($data['items']) && sizeof($data['items']) > 0) {
            // Load stocks by order items
            $this->loadStocks($data['items']);

            // Validate and assign stock info to order items
            $items = $this->validateItems($data['items']);

            if (!$items) {
                $this->addError([
                    'status'   => 400,
                    'detail'   => 'You just send invalid items!'
                ]);
                return $this->handleResult([]);
            }
            else {
                $data['items'] = $items;
            }
        }

        $data = $this->createData($data, $isDraft);
        if ($isDraft) {
            $res = $this->repository->createOrder($data);
            return $res;
        }

        $res = $this->group->createOrderGroup($data);

        return $this->handleResult($res);
    }

    public function update($data, $id)
    {
        $data = $this->updateData($data);
        $order = $this
            ->repository
            ->skipPresenter()
            ->with('status')
            ->findWhereFirst(['uuid' => $id]);

        if ($order->status->label === OrderStatus::DRAFT) {
            $res = $this
                ->repository
                ->updateDraftOrder($order, $data);
            return $res;
        }
        // We can not use presenter when
        // Machine update an order
        $res = $this
            ->repository
            ->updateOrder($order, $data);

        return $this->handleResult($res);
    }

    public function touch($data, $id)
    {
        $result = null;
        if(isset($data['toucherId'])) {
            $order = $this
                ->repository
                ->skipPresenter()
                ->findWhereFirst(['uuid' => $id]);
            if (!$order) {
                $this->addError([
                    'status' => 400,
                    'detail' => 'Touch order failed!'
                ]);
            } else {
                $order->touch();
                $toucherId = $data['toucherId'];
                unset($data['toucherId']);
                event(new OrderUpdated($order, $data, $toucherId));
                $result = [
                    'data' => app(OrderTransformer::class)->transform($order)
                ];
            }
        } else {
            $this->addError([
                'status' => 400,
                'detail' => 'ToucherId does not exist!'
            ]);
        }

        return $this->handleResult($result);
    }

    public function delete($id)
    {
        $res = $this
            ->repository
            ->skipPresenter()
            ->findWhereFirst(['uuid' => $id])
            ->delete();

        if (!$res) {
            $this->addError([
                'status'   => 400,
                'detail'   => 'Delete order failed!'
            ]);
        }
        return $this->handleResult([
            'data' => true
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function duplicate($id) {
        $res = $this->repository->duplicate($id);
        if (!$res) {
            $this->addError([
                'status'   => 400,
                'detail'   => 'Duplicate order failed!'
            ]);
        }
        return $this->handleResult($res);
    }

    public function addAttachments($id, $data) {
        $order = $this
            ->repository
            ->skipPresenter()
            ->findWhereFirst(['uuid' => $id]);

        if (!$order) {
            $this->addError([
                'status' => 404,
                'detail' => 'Order not found!'
            ]);
        }

        $res = $this->repository->addAttachments($order, $data, true);

        if (!$res) {
            $this->addError([
                'status' => 400,
                'detail' => 'Update order failed!'
            ]);
        }
        return $this->handleResult([
            'data' => $res
        ]);
    }

    public function deleteAttachments($id, $ids) {
        $order = $this
            ->repository
            ->skipPresenter()
            ->findWhereFirst(['uuid' => $id]);

        if (!$order) {
            $this->addError([
                'status' => 404,
                'detail' => 'Order not found!'
            ]);
        }

        $res = $this->repository->deleteAttachments($order, $ids, true);

        if (!$res) {
            $this->addError([
                'status' => 400,
                'detail' => 'Delete attachments failed!'
            ]);
        }
        return $this->handleResult([
            'data' => $res
        ]);
    }

    public function emailProforma($id) {
        $res = $this->repository->with(['status'])->skipPresenter()->findWhereFirst([
            'uuid' => $id
        ]);
        if (!isset($res)) {
            return [
                'errors' => [
                    'status'   => 404,
                    'detail'   => 'Order not found!'
                ]
            ];
        }

        // Load all stocks of the order
        $this->stockGateway->loadOrderStocks($res);

        // Load all products of the order
        $this->productGateway->loadOrderProducts($res);

        $data = $res->toArray();

        if (isset($data['delivery_term_id']) && $data['delivery_term_id'] != null) {
            $data['deliveryTerm'] = $res->deliveryTerms->name;
        }
        if (isset($data['delivery_method_id']) && $data['delivery_method_id'] != null) {
            $data['deliveryMethod'] = $res->deliveryMethod->name;
        }

        unset($data['delivery_term_id']);
        unset($data['delivery_method_id']);

        $items = [];
        foreach ($res->items as $item) {
            $itemData = $item->toArray();

            $stock = $this
                ->stockGateway
                ->findByLocation(
                    $item->product_id,
                    $item->location_code
                );

            $product = $this
                ->productGateway
                ->findById($item->product_id);

            $itemData = array_merge($itemData, [
                "sku"               => $stock->sku,
                "chilled_status"    => $stock->chilledStatus,
                "description"       => $stock->description,
                "cut_specification" => $product->cutSpecification,
            ]);

            $items[] = $itemData;
        }

        $data['items'] = $items;

        $this->emailQueueClient->send(
            'order.' . $id . '.' . Carbon::now()->toIso8601String(),
            'order.' . $id,
            [
                "isSystemEmail" => true,
                "receiverIDs" => [Request::user()->id],
                "scope" => "order.proforma",
                "data" => $data
            ]);
        return [
            'success' => [
                'status' => 200
            ]
        ];
    }

    /**
     * Order Summaries
     *
     * @return mixed
     */
    public function summary() {
        $this->repository->pushCriteria(
            app(OrderCriteria::class)->allStatuses()
        );
        $res = $this->repository->summary();
        return $res;
    }

    /**
     * Create one or more order items and attach to the given order.
     * 
     * @param \Illuminate\Http\Request $req
     * @param $id
     *
     * @return array
     */
    public function createOrderItems(\Illuminate\Http\Request $req, $id)
    {
        $order = $this->repository
            ->skipPresenter()
            ->findByField('uuid', $id)
            ->first();

        $updated = [];

        if ($order) {
            $allowedOrderStatus = OrderStatus::getOrderStatusIdsByLabels(
                [
                    OrderStatus::DRAFT,
                    OrderStatus::OFFER,
                    OrderStatus::PENDING,
                ]);

            if (in_array($order->order_status_id, $allowedOrderStatus)) {

                $this->loadStocks($req['items']);
                $items = $this->validateItems($req['items']);

                if ($items) {

                    $newItems = [];

                    foreach ($items as $item) {
                        $newItems[] = new OrderItem(OrderItem::fromCreateData($item));
                    }

                    $order->items()->saveMany($newItems);

                    $order->calculateTotals();
                    $updated = $order->getDirty();
                    $updated['items'] = $newItems;
                    $order->save();

                } else {
                    $this->addError([
                        'detail' => 'At least one item does not exist in inventory.'
                    ]);
                }
            } else {
                $this->addError([
                    'detail' => 'Order not in status allowed for item creation.'
                ]);
            }
        } else {
            $this->addError([
                'detail' => 'Could not find order: ' . $id
            ]);
        }

        $data = app(OrderTransformer::class)->transformUpdates($order, $updated);

        if (!empty($updated)) {
            event(new OrderUpdated($order, $updated, $this->request->user()->id));
        }

        return $this->handleResult(
            [
                'meta'=> [
                    'message' => 'Item(s) created.',
                    'code' => 200
                ],
                'data' => $data,
            ]
        );
    }

    /**
     * Update price and / or quantity on order items
     * 
     * @param \Illuminate\Http\Request $req
     * @param $id
     *
     * @return array
     */
    public function updateOrderItems(\Illuminate\Http\Request $req, $id)
    {
        $order = $this->repository
            ->skipPresenter()
            ->findByField('uuid', $id)
            ->first();

        $updated = [];

        if ($order) {
            $allowedOrderStatus = OrderStatus::getOrderStatusIdsByLabels(
                [
                    OrderStatus::DRAFT,
                    OrderStatus::OFFER,
                    OrderStatus::PENDING,
                ]);

            if (in_array($order->order_status_id, $allowedOrderStatus)) {

                // Make sure all 'id's are valid.
                foreach ($req['items'] as $item) {
                    $orderItem = $order->items()->where('id',$item['id'])->first();
                    if (!$orderItem) {
                        $this->addError([
                            'detail' => 'Could not find order item with id:' . $item['id']
                        ]);
                    }
                }

                if (empty($this->errors)) {
                    $updatedItems = [];

                    foreach ($req['items'] as $item) {

                        $orderItem = $order->items()->where('id',$item['id'])->first();

                        if (isset($item['price'])) {
                            $orderItem->unit_price = (float)$item['price'];
                        }

                        if (isset($item['quantity'])) {
                            $orderItem->quantity = $item['quantity'];
                        }

                        if ($orderItem->isDirty()) {
                            $updatedItems[] = $orderItem;
                        }

                        $orderItem->calculateTotals()->save();
                    }

                    $order->calculateTotals();
                    $updated = $order->getDirty();
                    if (!empty($updatedItems)) {
                        $updated['items'] = $updatedItems;
                    }
                    $order->save();
                }
            } else {
                $this->addError([
                    'detail' => 'Order not in status allowed for item updating.'
                ]);
            }
        } else {
            $this->addError([
                'detail' => 'Could not find order: ' . $id
            ]);
        }

        $this->stockGateway->loadOrderStocks($order);
        $data = app(OrderTransformer::class)->transformUpdates($order, $updated);

        if (!empty($updated)) {
            event(new OrderUpdated($order, $updated, $this->request->user()->id));
        }

        return $this->handleResult(
            [
                'meta'=> [
                    'message' => 'Item(s) updated.',
                    'code' => 200
                ],
                'data' => $data,
            ]
        );
    }

    /**
     * Delete order items.
     *
     * @param \Illuminate\Http\Request $req
     * @param $id
     *
     * @return array
     */
    public function deleteOrderItems(\Illuminate\Http\Request $req, $id)
    {
        $updated = [];
        if($order = $this->repository->skipPresenter()->findWhereFirst(['uuid' => $id])) {
            // allow draft or offer items to be deleted.
            $allowedOrderStatus = OrderStatus::getOrderStatusIdsByLabels([OrderStatus::DRAFT, OrderStatus::OFFER]);

            // check if order exists and is allowed.
            if(in_array($order->order_status_id, $allowedOrderStatus)) {
                if($order->items()->whereIn('id', $req['items'])->delete()) {
                    $order->calculateTotals();
                    $updated = $order->getDirty();
                    $updated['items'] = [];
                    foreach ($req['items'] as $item) {
                        $updated['items'][] = [
                            'id' => $item,
                            'deleted' => true,
                        ];
                    }
                    $order->save();
                } else {
                    $this->addError([
                        'status'   => 404,
                        'detail'   => 'Order item(s), ' . implode(", ", $req['items']) . ' do(es) not exist.'
                    ]);
                }
            } else {
                $this->addError([
                    'status'   => 404,
                    'detail'   => 'Order items are allowed to be deleted in draft or offer status.'
                ]);
            }
        } else {
            $this->addError([
                'status'   => 404,
                'detail'   => 'Unable to find order, ' . $id
            ]);
        }

        if (!empty($updated)) {
            event(new OrderUpdated($order, $updated, $this->request->user()->id));
        }

        return $this->handleResult(
            [
                'meta'=> [
                    'message' => 'Item(s) deleted.',
                    'code' => 200
                ],
                'data' => app(OrderTransformer::class)->transformUpdates($order,$updated),
            ]
        );
    }

    /**
     * @param $data
     * @param bool $isDraft
     * @return array
     */
    private function createData($data, $isDraft = false) {
        $result = [];
        $authorId = $data['userID'];
        $recipientAuthorIds = array_get($data, 'sendToUserIDs', []);
        $deliveryPostcode =  array_get($data, 'postCode');
        Helpers::unsetArray($data, ['userID', 'sendToUserIDs', 'postCode']);

        if ($isDraft) {
            $result = array_merge(Convention::snakeConvention($data), [
                'send_to_users'         => $recipientAuthorIds,
                'author_id'             => $authorId,
                'recipient_author_id'   => null,
                'delivery_postcode'     => $deliveryPostcode
            ]);
            return $result;
        }

        foreach ($recipientAuthorIds as $recipientID) {
            $result[] = array_merge(Convention::snakeConvention($data), [
                'author_id'             => $authorId,
                'recipient_author_id'   => $recipientID,
                'delivery_postcode'     => $deliveryPostcode,
            ]);
        }
        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    private function updateData(array $data) {
        if (isset($data['attributes']['recipientAuthors'])) {
            $data['attributes']['send_to_users'] = array_map(function ($author) {
                return $author['id'];
            }, $data['attributes']['recipientAuthors']);
        }
        return $data;
    }

    private function loadStocks($items = []) {
        $filters = [];
        foreach ($items as $item) {
            $filters[] = [
                'productUuid'       => $item['productId'],
                'facilityCode'      => $item['locationCode'],
                'productionDate'    => array_get($item,'productionDate',null)
            ];
        }
        $this->stockGateway->filter($filters);
    }

    /**
     * Validate order's items.
     * Find stock by productId and stock must be exist.
     * Assign price from stock to order item
     *
     * @param array $items
     * @return array|bool
     */
    private function validateItems($items = []) {
        foreach ($items as $key => $item) {
            $stock = $this->stockGateway->findByLocation(
                $item['productId'],
                $item['locationCode'],
                array_get($item,'productionDate',null)
            );

            // Find stock by productId and stock must be exist.
            if (!$stock) {
                return false;
            }

            // Assign price from stock to order item
            $item['price'] = $stock->price;
            if (isset($item['unitPrice'])) {
                $item['price'] = $item['unitPrice'];
            }
            $item['locationName'] = $stock->facility['name'];

            $items[$key] = $item;
        }

        return $items;
    }

    private function addError($error) {
        $this->errors[] = $error;
    }

    private function handleResult($res) {
        if (count($this->errors)) {
            return [
                'errors' => $this->errors
            ];
        }
        return $res;
    }
}
