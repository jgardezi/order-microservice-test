<?php
/**
 * Created by PhpStorm.
 * User: quyenhoang
 * Date: 2/17/19
 * Time: 3:23 PM
 */

namespace App\Gateways;


use App\Entities\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PXC\JsonApi\Client\MachineRequestInterface;

class StockGateway extends RemoteGateway
{
    private $client;
    private $org_uuid;

    /**
     * @var Collection
     */
    protected $data;

    /**
     * StockGateway constructor.
     * @param MachineRequestInterface $client
     * @param Request $request
     */
    public function __construct(MachineRequestInterface $client, Request $request)
    {
        $this->client = $client;
        $this->client->setBaseUri(env('API_GATEWAY_ENDPOINT'));
        $this->org_uuid = $request->input('org_uuid');
        parent::__construct();
    }

    public function setOrg($org_uuid)
    {
        // In the case of buyer's we want the org associated with the order, not user.
        $this->org_uuid = $org_uuid;
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function filter($ids) {
        $body = $this->makeRequestBody(['ids' => $ids]);
        try {
            $res = $this->client->post('stock/filter?org_uuid='.$this->org_uuid, $body,false);
        } catch(\Exception $e) {
            return null;
        }
        $this->cache($res);
        return $this->handleResponse($res);
    }

    /**
     * @param string $productId
     * @param string $locationCode
     * @param string $date
     * @return mixed
     */
    public function findByLocation(string $productId, string $locationCode, string $date = null) {
        $query = $this->query()
            ->where('productId', $productId)
            ->where('facility.code', $locationCode);
        if ($date) {
            $query = $query->where('productionDate', $date);
        }
        return $query->first();
    }

    public function loadOrderStocks(Orders $order) {
        if ($order->items()->count() > 0) {
            $items = [];
            $this->setOrg($order->org_id);
            foreach ($order->items as $item) {
                if ($item->production_dates && sizeof($item->production_dates) > 0) {
                    foreach ($item->production_dates as $date) {
                        $stock = $this->findByLocation(
                            $item->product_id,
                            $item->location_code,
                            $date
                        );
                        if (!$stock) {
                            $items[] = [
                                'productUuid'       => $item->product_id,
                                'facilityCode'      => $item->location_code,
                                'productionDate'    => $date
                            ];
                        }
                    }
                } else {
                    $stock = $this->findByLocation(
                        $item->product_id,
                        $item->location_code
                    );
                    if (!$stock) {
                        $items[] = [
                            'productUuid'       => $item->product_id,
                            'facilityCode'      => $item->location_code,
                        ];
                    }
                }
            }
            if (sizeof($items) > 0) {
                $this->filter($items);
            }
        }
    }

    public function checkStockAvailability(Orders $order)
    {
        $data = [];
        foreach ($order->items as $item) {

            $add = [
                'status' => $order->status->label,
                'quantityRequired' => $item->quantity,
                'productId' => $item->product_id,
                'facilityCode' => $item->location_code,
            ];

            if ($item->production_dates) {
                $add['productionDates'] = $item->production_dates;
            }

            $data[] = $add;
        }

        $request = [
            'data' => $data
        ];

        $body = $this->makeRequestBody($request);

        $response = $this->client->post('stock/check?org_uuid='.$order->org_id, $body, false);

        if (count($response->getErrors())) {
            return ['errors'=>$response->getErrors()];
        }

        $ret = $this->handleResponse($response);

        $available = true;
        foreach ($ret->items aS $item) {
            if ($item->available === false) {
                $available = false;
            }
        }
        $ret->allAvailable = $available;

        return $ret;
    }

    public function returnStock(Orders $order)
    {
        $data = [];
        foreach ($order->items as $item) {

            $add = [
                'status' => $order->status->label,
                'amountReturned' => $item->quantity,
                'productId' => $item->product_id,
                'facilityCode' => $item->location_code,
            ];

            if ($item->production_dates) {
                $add['productionDates'] = $item->production_dates;
            }

            $data[] = $add;
        }

        $request = [
            'data' => $data
        ];

        $body = $this->makeRequestBody($request);

        $response = $this->client->post('stock/return?org_uuid'.$order->org_id, $body, false);

        if (count($response->getErrors())) {
            $ret = ['errors'=>[]];
            foreach ( $response->getErrors() as $error ) {
                $ret['errors'][] = [
                    'detail' => $error->getDetail(),
                ];
            }

            return $ret;
        }

        $ret = $response->getMeta();

        return $ret;

    }
}