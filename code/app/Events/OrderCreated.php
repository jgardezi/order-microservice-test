<?php

namespace App\Events;

use App\Entities\Orders;
use App\Presenters\OrderPresenter;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderCreated extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;
    /**
     * @var Orders
     */
    public $order;

    /**
     * Create a new event instance.
     * @param Orders $order
     * @return void
     */
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    public function broadcastOn()
    {
        return 'orders.'.$this->order->org_id.'.created';
    }

    public function broadcastWith() {
        return app(OrderPresenter::class)->present($this->order);
    }
}
