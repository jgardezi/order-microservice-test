<?php

namespace App\Events;

use App\Entities\OrderGroup;

class AssignPXCReferenceIDToOrderEvent extends Event
{
    /**
     * @var OrderGroup
     */
    public $order;

    /**
     * Create a new event instance.
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }
}
