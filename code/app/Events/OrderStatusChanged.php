<?php

namespace App\Events;

use App\Entities\Orders;

class OrderStatusChanged extends Event
{
    /**
     * @var Orders
     */
    public $order;

    /**
     * @var string
     */
    public $oldStatus;

    /**
     * @var string
     */
    public $newStatus;

    /**
     * @var string
     */
    public $whoChanged;

    /**
     * Create a new event instance.
     * @param $oldStatus
     * @param $newStatus
     * @return void
     */
    public function __construct($order, $oldStatus, $newStatus, $whoChanged)
    {
        $this->order = $order;
        $this->oldStatus = $oldStatus;
        $this->newStatus = $newStatus;
        $this->whoChanged = $whoChanged;
    }
}
