<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserGeneralEvent implements ShouldBroadcast
{
    use InteractsWithSockets;

    public $userId;
    public $payload;
    public $eventName;

    /**
     * Create a new event instance.
     *
     * @param $userId
     * @param array $payload
     * @param string $eventName
     */
    public function __construct($userId, array $payload = [], $eventName = UserGeneralEvent::class)
    {
        $this->userId = $userId;
        $this->payload = $payload;
        $this->eventName = $eventName;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('users.' . $this->userId . '.notifications');
    }

    public function broadcastAs()
    {
        return $this->eventName;
    }

    public function broadcastWith()
    {
        return $this->payload;
    }
}
