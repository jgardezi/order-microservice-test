<?php

namespace App\Events;

use App\Entities\OrderGroup;

class AssignChannelToOrderEvent extends Event
{
    /**
     * @var OrderGroup
     */
    public $group;

    /**
     * Create a new event instance.
     * @param OrderGroup $group
     * @return void
     */
    public function __construct(OrderGroup $group)
    {
        $this->group = $group;
    }
}
