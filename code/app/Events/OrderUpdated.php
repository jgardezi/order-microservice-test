<?php

namespace App\Events;

use App\Entities\Orders;
use App\Transformers\OrderTransformer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderUpdated extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * @var Orders
     */
    public $order;
    public $updates;
    public $whoChanged;

    /**
     * Create a new event instance.
     *
     * @param Orders $order
     * @param array $updates
     */
    public function __construct(Orders $order, array $updates = [], $whoChanged)
    {
        $this->order = $order;
        $this->updates = $updates;
        $this->whoChanged = $whoChanged;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('orders.' . $this->order->uuid . '.updated');
    }

    public function broadcastWith()
    {
        return (app(OrderTransformer::class))->transformUpdates($this->order, $this->updates);
    }
}
