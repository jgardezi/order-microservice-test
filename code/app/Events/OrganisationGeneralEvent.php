<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrganisationGeneralEvent implements ShouldBroadcast
{
    use InteractsWithSockets;

    public $organisationId;
    public $payload;
    public $eventName;

    /**
     * Create a new event instance.
     *
     * @param $organisationId
     * @param array $payload
     * @param string $eventName
     */
    public function __construct($organisationId, array $payload = [], $eventName = OrganisationGeneralEvent::class)
    {
        $this->organisationId = $organisationId;
        $this->payload = $payload;
        $this->eventName = $eventName;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('organisations.' . $this->organisationId . '.notifications');
    }

    public function broadcastAs()
    {
        return $this->eventName;
    }

    public function broadcastWith()
    {
        return $this->payload;
    }
}
