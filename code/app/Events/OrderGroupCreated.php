<?php

namespace App\Events;

use App\Entities\OrderGroup;
use App\Entities\Orders;
use App\Presenters\OrderGroupPresenter;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderGroupCreated extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;
    /**
     * @var Orders
     */
    public $group;

    /**
     * @var
     */
    public $orgId;

    /**
     * Create a new event instance.
     * @param OrderGroup $group
     * @param string $orgId
     * @return void
     */
    public function __construct(OrderGroup $group, $orgId)
    {
        $this->orgId = $orgId;
        $this->group = $group;
    }

    public function broadcastOn()
    {
        return 'order_group.'.$this->orgId.'.created';
    }

    public function broadcastWith() {
        return app(OrderGroupPresenter::class)->present($this->group);
    }
}
