<?php
namespace App\Http\Validators;

use Illuminate\Http\Request;

abstract class Validator {
    /**
     * Rules
     */
    protected $rules = [];

    /**
     * Messages
     */
    protected $messages = [];

    /**
     * Validator
     */
    protected $validator;

    /**
     * Error code
     */
    protected $statusCode = 400;

    /**
     * Validate
     */
    public function validate(Request $request) {
        $this->validator = app('validator')->make($request->all(), $this->rules, $this->messages);
        return !$this->validator->fails();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getErrorMessages() {
        if (!$this->validator) {
            throw new \Exception('You need validate before getting the errors!');
        }
        $errors = [];
        foreach ($this->validator->errors()->toArray() as $key => $messages) {
            $errors[] = [
                'status' => $this->statusCode,
                'detail' => $messages[0],
                'source' => [ 'pointer' => $key ],
                'ValidatorError' => true
            ];
        }
        return $errors;
    }

    public function setRules($rules = []) {
        $this->rules = array_merge($this->rules, $rules);
    }

    public function setStatusCode($code = 400) {
        $this->statusCode = $code;
    }

    public function getStatusCode() {
        return $this->statusCode;
    }
}