<?php
namespace App\Http\Validators;

use App\Constants\UserRole;
use App\Entities\Orders;
use App\Entities\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateOrder extends Validator {
    /**
     * Rules
     */

    protected $rules = [
        'attributes'                                    => 'required|array',
        'attributes.recipientAuthors'                   => 'requiredField:state,offer,pending|array',
        'attributes.subject'                            => 'requiredIfExists|string',
        'attributes.note'                               => 'string',
        'attributes.deliveryMethod'                     => 'requiredField:attributes.state,pending|array',
        'attributes.deliveryMethod.id'                  => 'requiredField:attributes.state,pending|string',
        'attributes.deliveryDate'                       => 'requiredField:attributes.state,pending|string',
        'attributes.deliveryTerm'                       => 'requiredField:attributes.state,pending|array',
        'attributes.deliveryTerm.id'                    => 'requiredField:attributes.state,pending|numeric',
        'attributes.shipmentPortName'                   => 'string',
        'attributes.deliveryAddress'                    => 'requiredField:attributes.state,pending|string',
        'attributes.locality'                           => 'string',
        'attributes.country'                            => 'requiredField:attributes.state,pending|array',
        'attributes.country.id'                         => 'requiredField:attributes.state,pending|numeric',
        'attributes.paymentTerms'                       => 'requiredField:attributes.state,pending|string',
        'attributes.termsOfSales'                       => 'requiredField:attributes.state,pending|string',
        'attributes.externalReferenceID'                => 'numeric',
        'attributes.poReference'                        => 'string',
        'attributes.currencyCode'                       => 'string',
        'attributes.postCode'                           => 'string',
        'attributes.channelId'                          => 'string',
        'attributes.sellerReference'                    => 'string',
        'attributes.attachments'                        => 'array',
        'attributes.attachments.*.userId'               => 'required|string',
        'attributes.attachments.*.attachmentId'         => 'required|string',
    ];

    /**
     * Messages
     */
    protected $messages = [
        //
    ];

    public function __construct()
    {
        $this->setRules([
            'attributes.state' => [
                'requiredIfExists',
                'string',
                Rule::in([
                    OrderStatus::OFFER, OrderStatus::DRAFT,
                    OrderStatus::PENDING, OrderStatus::CANCELLED,
                    OrderStatus::CLOSED, OrderStatus::CONFIRMED
                ])
            ]
        ]);
    }

    public function validate(Request $request)
    {
        $valid = parent::validate($request);

        if ($valid) {
            $user = $request->user();

            $order = Orders::query()
                ->where('uuid', $request->route('id'))->first();
            $attributes = $request->get('attributes');

            if (!isset($order)) {
                $this->setStatusCode(404);
                $this->validator->errors()->add('id', 'Order not found!');
                return false;
            }

            $label = $order->status->label;
            $state = isset($attributes['state']) ? $attributes['state'] : false;
            $isDraft = ($order->status->label === OrderStatus::DRAFT);

            // No other user can edit other user's draft.
            if ($isDraft && ($user->id !== $order->author_id)) {
                $this->validator->errors()->add('state', 'You do not have permission for update this order!');
                return false;
            }

            // Not allow update section 1. (Summary)
            if (!$isDraft) {
                unset($attributes['note']);
                unset($attributes['subject']);

                $request->merge([
                    'attributes' => $attributes
                ]);
            }

            if ($isDraft && ($state === OrderStatus::OFFER || $state === OrderStatus::PENDING)) {
                if (
                    ($order->items->count() === 0) &&
                    (!isset($attributes['items']) || sizeof($attributes['items']) === 0)
                )
                {
                    $this->validator->errors()->add('items', 'Order is required at least 1 product!');
                    return false;
                }

                if (
                    (!isset($order->send_to_users) || sizeof($order->send_to_users) === 0) &&
                    (!isset($attributes['recipientAuthors']) || sizeof($attributes['recipientAuthors']) === 0)
                )
                {
                    $this->validator->errors()->add('recipientAuthors', 'Order is required at least 1 recipient!');
                    return false;
                }
            }

            // 1. Buyer
            if ($user->id === $order->recipient_author_id) {
                // 2.1: Only buyer can confirmed order.
                $buyerAllowedUpdates = [];
                if (
                    $state
                    && $label === OrderStatus::PENDING
                    && $state === OrderStatus::CONFIRMED
                ) {
                    $buyerAllowedUpdates['state'] = $state;
                }

                // Buyer can update poReference
                if (isset($attributes['poReference'])) {
                    $buyerAllowedUpdates['poReference'] = $attributes['poReference'];
                }

                if (count($buyerAllowedUpdates)) {
                    $request->merge([
                        'attributes' => $buyerAllowedUpdates
                    ]);
                    return true;
                }

                $this->validator->errors()->add('state', 'You do not have permission for update this order!');
                return false;
            }

            // 2. Seller or Owner or Employee
            if (
                $user->id === $order->author_id
                || $user->role === UserRole::OWNER
                || $user->role === UserRole::EMPLOYEE
            ) {
                // 2.1. States are available for the order.
                $attributes = [];
                if ($state && $state !== $label) {
                    $states = [];
                    switch ($label) {
                        case OrderStatus::DRAFT:
                            $states = [OrderStatus::OFFER, OrderStatus::PENDING];
                            break;
                        case OrderStatus::OFFER:
                            $states = [OrderStatus::PENDING];
                            break;
                        case OrderStatus::PENDING:
                            $states = [OrderStatus::OFFER, OrderStatus::CONFIRMED, OrderStatus::CANCELLED];
                            break;
                        case OrderStatus::CONFIRMED:
                            $states = [OrderStatus::CANCELLED, OrderStatus::CLOSED];
                            break;
                        case OrderStatus::CLOSED:
                            $states = [OrderStatus::CANCELLED];
                    }

                    // Note: Maybe state is not available for the order.
                    if (!in_array($state, $states)) {
                        $this->validator->errors()->add('state', "Can not update state of this order to $state! Can be:" . implode(", ", $states));
                        return false;
                    }
                    $attributes['state'] = $state;
                }

                // 2.2. Only allow update seller ref, po ref, or delivery date
                if ($label !== OrderStatus::DRAFT && $label !== OrderStatus::OFFER) {
                    if ($request->has('attributes.poReference')) {
                        $attributes['poReference'] = $request->get('attributes')['poReference'];
                    }

                    if ($request->has('attributes.externalReferenceId')) {
                        $attributes['externalReferenceId'] = $request->get('attributes')['externalReferenceId'];
                    }

                    if ($request->has('attributes.deliveryDate')) {
                        $attributes['deliveryDate'] = $request->get('attributes')['deliveryDate'];
                    }

                    if ($request->has('attributes.sellerReference')) {
                        $attributes['sellerReference'] = $request->get('attributes')['sellerReference'];
                    }

                    $request->merge([
                        'attributes' => $attributes,
                    ]);
                }
            }
        }

        return $valid;
    }
}