<?php
namespace App\Http\Validators;

class QueryOrder extends Validator {
    /**
     * Rules
     */
    protected $rules = [
        'page'      => 'integer',
        'limit'     => 'integer',
        'channelId' => 'string',
        'sort'      => 'string|in:asc,desc'
    ];

    /**
     * Messages
     */
    protected $messages = [];
}