<?php
namespace App\Http\Validators;

use App\Entities\Orders;
use App\Entities\OrderStatus;
use Illuminate\Http\Request;

class DeleteOrder extends Validator {
    /**
     * Rules
     */

    protected $rules = [];

    /**
     * Messages
     */
    protected $messages = [
        //
    ];

    public function validate(Request $request)
    {
        $valid = parent::validate($request);
        if ($valid) {
            $user = $request->user();
            $order = Orders::query()
                ->where('uuid', $request->route('id'))->first();

            if (!isset($order)) {
                $this->setStatusCode(404);
                $this->validator->errors()->add('id', 'Order not found!');
                return false;
            }

            if ($order->author_id !== $user->id) {
                $this->setStatusCode(403);
                $this->validator->errors()->add('id', 'You don\'t have permission to delete this order!');
                return false;
            }

            if ($order->status->label !== OrderStatus::DRAFT) {
                $this->setStatusCode(403);
                $this->validator->errors()->add('id', 'You can not delete this order!');
                return false;
            }
        }

        return $valid;
    }
}