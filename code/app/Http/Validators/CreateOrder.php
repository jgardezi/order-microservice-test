<?php
namespace App\Http\Validators;

use App\Entities\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CreateOrder extends Validator {
    /**
     * Rules
     */
    protected $rules = [
        'sendToUserIDs'             => 'required_if:state,offer,pending|array',
        'subject'                   => 'required_if:state,offer,pending|string',
        'note'                      => 'string',
        'items'                     => 'required_if:state,offer,pending|array',
        'items.*.productId'         => 'required|string',
        'items.*.quantity'          => 'required|integer',
//        'items.*.price'             => 'required_if:state,offer,pending|numeric',
        'items.*.locationCode'      => 'required|string',
        'deliveryMethodID'          => 'required_if:state,pending|integer',
        'deliveryDate'              => 'required_if:state,pending|date',
        'deliveryTermID'            => 'required_if:state,pending|integer',
        'shipmentPortName'          => 'string',
        'deliveryAddress'           => 'required_if:state,pending|string',
        'locality'                  => 'string',
        'countryID'                 => 'required_if:state,pending|integer',
        'paymentTerms'              => 'required_if:state,pending|string',
        'termsOfSales'              => 'required_if:state,pending|string',
        'externalReferenceID'       => 'numeric',
        'poReference'               => 'string',
        'currencyCode'              => 'string',
        'postCode'                  => 'string',
        'channelId'                 => 'string',
        'sellerReference'           => 'string',
        'attachments'               => 'array',
    ];

    /**
     * Messages
     */
    protected $messages = [
        //
    ];

    public function __construct()
    {
        $this->setRules([
            'state' => [
                'required',
                'string',
                Rule::in([
                    OrderStatus::OFFER, OrderStatus::DRAFT, OrderStatus::PENDING
                ])
            ]
        ]);
    }

    public function validate(Request $request)
    {
        $valid = parent::validate($request);
        if ($valid) {
            $data = $request->only(['state', 'channelId', 'sendToUserIDs']);

            if ($data['state'] === OrderStatus::DRAFT) {
                return true;
            }

            if (isset($data['channelId']) && sizeof($data['sendToUserIDs']) !== 1) {
                $this->validator->errors()->add('sendToUserIDs', 'Offer in external channel is only allow one buyer!');
                return false;
            }

            if ($data['state'] === OrderStatus::PENDING && sizeof($data['sendToUserIDs']) > 1) {
                $this->validator->errors()->add('sendToUserIDs', 'Order is only allow one buyer!');
                return false;
            }
        }
        return $valid;
    }
}