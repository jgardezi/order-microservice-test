<?php
namespace App\Http\Validators;

use App\Constants\UserRole;
use App\Entities\Orders;
use App\Entities\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DuplicateOrder extends Validator {
    /**
     * Rules
     */

    protected $rules = [];

    /**
     * Messages
     */
    protected $messages = [
        //
    ];

    public function validate(Request $request)
    {
        $valid = parent::validate($request);
        if ($valid) {
            $user = $request->user();
            $order = Orders::query()
                ->where('uuid', $request->route('id'))->first();

            if (!isset($order)) {
                $this->setStatusCode(404);
                $this->validator->errors()->add('id', 'Order not found!');
                return false;
            }

            if ($order->author_id !== $user->id) {
                $this->validator->errors()->add('id', 'You don\'t have permission to duplicate this order!');
                return false;
            }
        }

        return $valid;
    }
}