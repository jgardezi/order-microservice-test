<?php

namespace App\Http\Requests;

class DeleteOrderItems extends JsonApiRequestAbstract
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "items" => "required|array|min:1",
            "items.*" => "required|int|distinct|min:1"
        ];
    }
}
