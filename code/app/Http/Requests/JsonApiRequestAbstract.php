<?php
/**
 * Created by PhpStorm.
 * User: nigel
 * Date: 2019-03-26
 * Time: 10:15
 */

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Pearl\RequestValidate\RequestAbstract;

/**
 * Class JsonApiRequestAbstract
 *
 * Abstract request validation, with errors formatted for
 * JSON+API spec.
 *
 * @package App\Http\Requests
 */
class JsonApiRequestAbstract extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Format validation errors in a fashion acceptable to JSON+API spec.
     *
     * @param Validator $validator
     * @return JsonResponse
     */
    protected function formatErrors(Validator $validator)
    {
        $errors = $validator->getMessageBag()->toArray();

        /**
         * the "errors" key must not appear with "data" and must be
         * an array.
         */
        return new JsonResponse(
            [
                'errors' => [
                    $errors
                ]
            ],
            422);
    }

}