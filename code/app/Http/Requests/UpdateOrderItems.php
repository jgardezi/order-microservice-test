<?php

namespace App\Http\Requests;

class UpdateOrderItems extends JsonApiRequestAbstract
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "items" => "required|array|min:1",
            "items.*" => "required|array|min:1",
            "items.*.id" => "required|int|min:1",
            "items.*.price" => "numeric",
            "items.*.quantity" => "numeric"
        ];
    }
}
