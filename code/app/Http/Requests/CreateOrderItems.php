<?php

namespace App\Http\Requests;

class CreateOrderItems extends JsonApiRequestAbstract
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "items" => "required|array|min:1",
            "items.*" => "required|array|min:1",
            "items.*.productId" => "required|string",
            "items.*.locationCode" => "required|string",
            "items.*.quantity" => "required|numeric"
        ];
    }
}
