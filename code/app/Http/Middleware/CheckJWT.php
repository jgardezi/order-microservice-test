<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use Closure;
use Illuminate\Http\Request;

class CheckJWT
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $auth = app(AuthService::class);
        $accessToken = $request->bearerToken();

        try {
            $tokenInfo = $auth->decodeJWT($accessToken);
        } catch (\Exception $e) {
            return response()->json(["message" => "Token Exception."], 401);
        }

        // `uid` will only existing for user token. Machine-to-Machine might not have uid.
        if(property_exists($tokenInfo, 'uid')) {
            $request->merge([
                'org_uuid'    => $tokenInfo->org,
                'userID'    => $tokenInfo->uid,
                'pxc.token' => $accessToken,
                'is_machine' => false,
            ]);

            // Note: This is required for authorize broadcast channel
            $request->setUserResolver(function () use ($tokenInfo) {
                $user = new \stdClass();
                $user->id = $tokenInfo->uid;
                $user->org_id = $tokenInfo->org;
                $user->role = $tokenInfo->usr_role;
                return $user;
            });
        }
        else {
            $request->setUserResolver(function () use ($tokenInfo) {
                return null;
            });
            $request->merge([
                'is_machine' => true,
            ]);
        }
        return $next($request);
    }
}