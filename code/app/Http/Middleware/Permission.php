<?php

namespace App\Http\Middleware;
use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $roles
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $user = $request->user();
        $roles = explode('|', $roles);

        if (
            ($user && in_array($user->role, $roles))
            || ($request->get('is_machine') && in_array('machine', $roles))
        ) {
            return $next($request);
        }
        $data = [
            'errors' => [
                [
                    "status" => 403,
                    "detail" => "Permission denied!",
                    "source" => null,
                    "PermissionDenied" => true,
                ]
            ],
        ];
        return response()->json(
            $data, 403, [], JSON_PRETTY_PRINT
        );
    }
}
