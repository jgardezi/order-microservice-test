<?php

namespace App\Http\Middleware;
use Closure;

class Validate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $validatorName
     * @return mixed
     */
    public function handle($request, Closure $next, $validatorName)
    {
        $v = app('validators.'.$validatorName);
        if (!$v->validate($request)) {
            $data = [
                'errors' => $v->getErrorMessages(),
            ];
            return response()->json(
                $data, $v->getStatusCode(), [], JSON_PRETTY_PRINT
            );
        }
        return $next($request);
    }
}
