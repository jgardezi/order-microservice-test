<?php

namespace App\Http\Controllers;

use App\Gateways\UserOrderGateway;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
    /**
     * @var \App\Gateways\OrderGateway
     */
    protected $gateway;

    public function __construct(UserOrderGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $res = $this->gateway->filter();
        if (!empty($res['errors'])) {
            return $this->responseBadRequest($res);
        }
        return $res;
    }

    public function offers() {
        $res = $this->gateway->offers();
        if (!empty($res['errors'])) {
            return $this->responseBadRequest($res);
        }
        return $res;
    }

    public function myTeamMembers(Request $request) {
        $user = $request->user();
        return $this->gateway->teamMembers($user);
    }

    public function myTeamOrders(Request $request)
    {
        $limit = 10;
        if ($request->has('limit')) {
            $limit = $request->get('limit');
        }
        $res = $this->gateway->teamOrders($limit);
        return $res;
    }
}