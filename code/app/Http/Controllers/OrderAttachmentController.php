<?php

namespace App\Http\Controllers;

use App\Gateways\OrderGateway;
use Illuminate\Http\Request;

class OrderAttachmentController extends Controller
{
    /**
     * @var \App\Gateways\OrderGateway
     */
    protected $gateway;

    public function __construct(OrderGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function store(Request $request, $id) {
        $res = $this->gateway->addAttachments($id, $request->get('attachments'));
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $res;
    }

    public function destroy(Request $request, $id) {
        $res = $this->gateway->deleteAttachments($id, $request->get('attachmentIds'));
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $res;
    }
}