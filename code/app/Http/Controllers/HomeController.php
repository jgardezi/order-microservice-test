<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Don't remove this method as its is used by ALB for app health checking.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            'message' => 'Order MicroServices'
        ]);
    }
}