<?php

namespace App\Http\Controllers;

use App\Repositories\OrdersRepository;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    private $orderRepository;

    public function __construct(OrdersRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Broadcasting authenticate.
     *
     * @param Request $request
     * @return array
     */
    public function authenticate(Request $request)
    {
        $data = $request->all();
        if (isset($data["scope"])) {
            if ($data["scope"] === "order.updated") {
                return $this->subscribeToOrderNotification($data);
            }
        }
        return response()->json([
            'errors' => 'unable to subscribe'
        ], 401);
    }

    public function subscribeToOrderNotification($data)
    {
        if (isset($data['userId']) && isset($data['orderId']) && isset($data['organisationId']) && isset($data['role'])) {
            $orderId = $data['orderId'];
            $userId = $data['userId'];
            // TODO the machine call can not perform fetch user information action.
            // We need to implement the "check if user is 'owner'" of an organisation that associated
            // with the order later when we make the machine call works
//            $organisationId = $data['organisationId'];
            $role = $data['role'];
            $order = $this->orderRepository
                ->skipPresenter()
                ->findWhereFirst([
                    'uuid' => $orderId
                ]);
            if(empty($order)) {
                return response()->json([
                    'errors' => 'unable to subscribe'
                ], 401);
            }
            // need to update the last condition when we make the user service works
            if ($order->author_id === $userId || $order->recipient_author_id === $userId || $role === 'owner') {
                return response()->json([
                    'meta' => [
                        'message' => 'success to subscribe'
                    ]
                ]);
            } else {
                return response()->json([
                    'errors' => 'unable to subscribe'
                ], 401);
            }
        }
    }
}