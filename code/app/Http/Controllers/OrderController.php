<?php

namespace App\Http\Controllers;

use App\Gateways\OrderGateway;
use App\Supports\IncludesParser;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var \App\Gateways\OrderGateway
     */
    protected $gateway;
    protected $includes;

    public function __construct(OrderGateway $gateway, IncludesParser $includes)
    {
        $this->gateway = $gateway;
        $this->includes = $includes;
        $this->includes->addDefaultInclude('items');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->includes->removeDefaultInclude('items');
        $limit = 10;
        if ($request->has('limit')) {
            $limit = $request->get('limit');
        }
        $res = $this->gateway->filter($limit);
        return $this->responseOK($res);
    }

    /**
     * Return all offers
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function offers() {
        $res = $this->gateway->offers();
        if (!empty($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * Store a newly created order in storage.
     */
    public function store(Request $request)
    {
        $this->includes->addDefaultInclude('attachments');
        $res = $this->gateway->store($request->all());
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * Display the specified order.
     *
     * @param $id
     * @return array
     */
    public function show($id)
    {
        $this->includes->addDefaultInclude('attachments');
        $res = $this->gateway->get($id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * Update the specified order in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $res = $this->gateway->update($request->all(), $id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    public function touch(Request $request, $id)
    {
        $payload = $request->all();
        // TODO is there a better way for doing this?
        unset($payload['is_machine']);
        unset($payload['pxc.token']);
        unset($payload['userID']);
        unset($payload['org_uuid']);
        $res = $this->gateway->touch($payload, $id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * Remove the specified order from storage.
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $res = $this->gateway->delete($id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function duplicate($id) {
        $res = $this->gateway->duplicate($id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }

    /**
     * @return mixed
     */
    public function summary()
    {
        $res = $this->gateway->summary();
        return $this->responseOK($res);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function emailProforma($id)
    {
        $res = $this->gateway->emailProforma($id);
        if (isset($res['errors'])) {
            return $this->responseError($res);
        }
        return $this->responseOK($res);
    }
}