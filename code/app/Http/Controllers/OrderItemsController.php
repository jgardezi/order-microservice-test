<?php

namespace App\Http\Controllers;

use App\Gateways\OrderGateway;
use App\Http\Requests\CreateOrderItems;
use App\Http\Requests\DeleteOrderItems;
use App\Http\Requests\UpdateOrderItems;

class OrderItemsController extends Controller
{
    /**
     * @var OrderGateway
     */
    protected $orderGateway;

    public function __construct(OrderGateway $orderGateway)
    {
        $this->orderGateway = $orderGateway;
    }

    /**
     * Create order items.
     *
     * @param CreateOrderItems $request
     * @param $id
     * @return array
     */
    public function create(CreateOrderItems $request, $id)
    {
        return $this->orderGateway->createOrderItems($request, $id);
    }

    /**
     * Update price and / or quantity on one or more order items.
     *
     * @param UpdateOrderItems $request
     * @param $id
     * @return array
     */
    public function update(UpdateOrderItems $request, $id)
    {
        return $this->orderGateway->updateOrderItems($request, $id);
    }

    /**
     * Delete order items.
     *
     * @param DeleteOrderItems $request
     * @param $id
     * @return array
     */
    public function delete(DeleteOrderItems $request, $id)
    {
        return $this->orderGateway->deleteOrderItems($request, $id);
    }
}