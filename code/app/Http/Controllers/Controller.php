<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function responseOK($data)
    {
        $data = $this->adjustPaginationLinks($data);
        return response()->json(
            $data, 200, [], JSON_PRETTY_PRINT
        );
    }

    protected function responseBadRequest($data, $code = 400)
    {
        return response()->json(
            $data, $code, [], JSON_PRETTY_PRINT
        );
    }

    protected function responseError($data)
    {
        $errors = $data['errors'];
        return response()->json(
            ['errors' => [$errors]], array_get($errors,'status',400), [], JSON_PRETTY_PRINT
        );
    }
    protected function adjustPaginationLinks($ret)
    {
        $ret = $this->adjustPaginationLink($ret, 'next');
        $ret = $this->adjustPaginationLink($ret, 'previous');

        return $ret;
    }

    protected function adjustPaginationLink($ret, $key)
    {
        $link = array_get($ret,'meta.pagination.links.'.$key);

        if ($link) {
            $parts = parse_url($link);

            $parameters = [];
            foreach( explode('&', $parts['query']) as $parm) {
                list($pkey, $value) = explode('=', $parm);
                $parameters[$pkey] = $value;
            }

            // Zap paras that come from the Auth token.
            if (array_get($parameters,'is_machine',0) == 0) {
                unset($parameters['org_uuid']);
            }
            unset($parameters['userID']);
            unset($parameters['pxc.token']);
            unset($parameters['is_machine']);

            // Put back together without host to make relative url.
            $query_parts = [];
            foreach ($parameters as $pkey => $param) {
                $query_parts[] = $pkey . '=' . $param;
            }

            $link = $parts['path'] . '?' . implode('&', $query_parts);

            array_set($ret, 'meta.pagination.links.'.$key, $link);
        }

        return $ret;
    }
}
