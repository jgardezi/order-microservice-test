<?php

namespace App\Http\Controllers;

use App\Repositories\ShippingRepository;
use App\Repositories\TermRepository;

class FieldsController
{
    /**
     * @var \App\Repositories\TermRepository
     */
    protected $termRepository;

    /**
     * @var \App\Repositories\ShippingRepository
     */
    protected $shippingRepository;

    /**
     * FieldsController constructor.
     *
     * @param \App\Repositories\ShippingRepository $shippingRepository
     * @param \App\Repositories\TermRepository $termRepository
     */
    public function __construct(
        ShippingRepository $shippingRepository,
        TermRepository $termRepository
    )
    {
        $this->shippingRepository = $shippingRepository;
        $this->termRepository = $termRepository;
    }

    public function terms()
    {
        return $this->termRepository->all();
    }

    public function shipping()
    {
        return $this->shippingRepository->all();
    }
}