<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/2/19
 * Time: 12:02 PM
 */
namespace App\Presenters;

use App\Transformers\OrderTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class OrderPresenter extends FractalPresenter
{
    /**
     * @return OrderTransformer|\League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return app(OrderTransformer::class);
    }
}
