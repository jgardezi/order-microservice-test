<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/2/19
 * Time: 12:02 PM
 */
namespace App\Presenters;


use App\Transformers\OrderItemTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class OrderItemPresenter extends FractalPresenter
{
    public function getTransformer()
    {
        return new OrderItemTransformer();
    }
}