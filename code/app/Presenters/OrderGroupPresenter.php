<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/8/19
 * Time: 2:50 PM
 */
namespace App\Presenters;


use App\Criteria\TeamOrderCriteria;
use App\Repositories\OrdersRepository;
use App\Transformers\OrderGroupTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class OrderGroupPresenter extends FractalPresenter
{
    /**
     * @var OrdersRepository
     */
    protected $orderPresenter;

    public function __construct(OrderPresenter $orderPresenter)
    {
        $this->orderPresenter = $orderPresenter;
        parent::__construct();
    }

    public function getTransformer()
    {
        return new OrderGroupTransformer($this->orderPresenter);
    }
}