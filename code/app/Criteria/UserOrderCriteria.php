<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserOrderCriteria extends OrderTypeCriteria implements CriteriaInterface {
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
        parent::__construct($request);
    }

    public function apply($model, RepositoryInterface $repository) {
        $model = parent::apply($model, $repository);
        if ($this->request->has('userID')) {
            $userId = $this->request->route('userId');
            $model = $model
                ->where('author_id', '=', $userId)
                ->orWhere('recipient_author_id', '=', $userId);
        }
        return $model;
    }
}