<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class TeamOrderCriteria implements CriteriaInterface {
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository) {

        if ($this->request->has('userIDs')) {
            $model = $model->with(['orders' => function ($query) {
                $query->whereIn('author_id',  $this->request->get('userIDs'));
            }])
            ->whereHas('orders', function ($query) {
                $query->whereIn('author_id',  $this->request->get('userIDs'));
            });
        }
        return $model;
    }
}