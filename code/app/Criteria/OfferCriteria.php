<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use App\Entities\OrderStatus;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OfferCriteria implements CriteriaInterface {
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository) {
        // All activity - this is all the orders for the user's own organisation
        if ($this->request->has('org_uuid')) {
            $model = $model->where([
                ['org_id',          '=', $this->request->get('org_uuid')],
                ['order_status_id', '=', OrderStatus::offer()],
                ['order_group_id',  '<>', null]
            ]);
        }

        return $model;
    }
}