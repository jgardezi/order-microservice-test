<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use App\Constants\UserRole;
use App\Entities\OrderStatus;
use App\Gateways\UserGateway;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderCriteria implements CriteriaInterface {
    protected $request;
    protected $user_gateway;
    protected $allStatuses;

    use GetOrganisationUsersTrait;

    public function __construct(Request $request, UserGateway $user_gateway) {
        $this->request = $request;
        $this->user_gateway = $user_gateway;
        $this->allStatuses = false;
    }

    public function allStatuses()
    {
        $this->allStatuses = true;
        return $this;
    }

    public function apply($model, RepositoryInterface $repository) {
        $user = $this->request->user();
        // 1. if user is owner, will get orders in their organisation.
        if ($user->role === UserRole::OWNER) {
            $ids = $this->getOrganisationUserIds();
            $model = $model->where(function($query) use ($ids) {
                $query->whereIn('author_id', $ids)
                    ->orWhere(function ($query) use ($ids) {
                        return $query->whereIn('recipient_author_id', $ids);
                    });
            });

        }
        // 2. Get all orders of the user.
        else {
            $model = $model
                ->where(function ($query) use ($user) {
                    return $query
                        ->where('author_id', '=', $user->id)
                        ->orWhere(function ($query) use ($user) {
                            return $query
                                ->where('recipient_author_id', '=', $user->id)
                                ->where('order_status_id', '<>', OrderStatus::draft());
                        });
                });
        }

        if ($this->request->has('channelId')) {
            $model = $model->where('channel_id', '=', $this->request->get('channelId'));
        }

        if ($this->request->has('orderType')) {
            $orderTypeLabels = explode(',',$this->request->get('orderType'));
            if (count($orderTypeLabels) === 1) {
                $orderType = OrderStatus::getByLabel($orderTypeLabels[0]);
                $model = $model->where('order_status_id', '=', $orderType->id);
            } else {
                $statuses = OrderStatus::getOrderStatusIdsByLabels($orderTypeLabels);
                $model = $model->whereIn('order_status_id', $statuses);
            }
        } else {
            // "All Tab" has draft, offer, pending and confirmed
            if (!$this->allStatuses) {
                $statuses = OrderStatus::getOrderStatusIdsByLabels([
                    OrderStatus::DRAFT, OrderStatus::OFFER, OrderStatus::PENDING, OrderStatus::CONFIRMED,
                ]);
                $model = $model->whereIn('order_status_id',$statuses);
            }
        }

        return $model;
    }

}