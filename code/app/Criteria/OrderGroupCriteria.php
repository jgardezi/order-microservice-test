<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use App\Constants\UserRole;
use App\Entities\OrderStatus;
use App\Gateways\UserGateway;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderGroupCriteria implements CriteriaInterface {
    protected $request;
    protected $user_gateway;

    use GetOrganisationUsersTrait;

    public function __construct(Request $request, UserGateway $user_gateway) {
        $this->request = $request;
        $this->user_gateway = $user_gateway;
    }

    public function apply($model, RepositoryInterface $repository) {
        $user = $this->request->user();
        // 1. if user is owner, will get orders in their organisation.
        if ($user->role === UserRole::OWNER) {
            $model = $model->whereHas('orders', function ($query) {
                $ids = $this->getOrganisationUserIds();
                return $query->whereIn('author_id', $ids)
                    ->orWhere(function($query) use ($ids){
                        return $query->whereIn('recipient_author_id', $ids);
                    });
            });
        }
        // 2. Get all orders of the user.
        else {
            $model = $model->with([
                'orders' => function ($query) use ($user) {
                    $query
                        ->where('author_id', '=', $user->id)
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('recipient_author_id', '=', $user->id)
                                ->where('order_status_id', '<>', OrderStatus::draft());
                        });
                }
            ]);

            $model = $model->whereHas('orders', function ($query) use ($user) {
                $query
                    ->where('author_id', '=', $user->id)
                    ->orWhere(function ($query) use ($user) {
                        $query
                            ->where('recipient_author_id', '=', $user->id)
                            ->where('order_status_id', '<>', OrderStatus::draft());
                    });
            });
        }

        if ($this->request->has('channelId')) {
            $model = $model->whereHas('orders', function ($query) {
                return $query->where('channel_id', '=', $this->request->get('channelId'));
            });
        }

        if ($this->request->has('orderType')) {
            $orderTypeLabels = explode(',',$this->request->get('orderType'));
            if (count($orderTypeLabels) === 1) {
                $model = $model->whereHas('orders', function ($query) use ($orderTypeLabels) {
                    $orderType = OrderStatus::getByLabel($orderTypeLabels[0]);
                    return $query->where('order_status_id', '=', $orderType->id);
                });
            } else {
                $model = $model->whereHas('orders', function ($query) use ($orderTypeLabels) {
                    $statuses = OrderStatus::getOrderStatusIdsByLabels($orderTypeLabels);
                    return $query->whereIn('order_status_id', $statuses);
                });
            }
        } else {
            if (!$this->request->has('channelId')) {
                /* We are on the "All Orders" channel, on the "All" tab.
                   Retrieve only groups with orders in Draft, Offer, Pending, or Confirmed.
                */

                $model = $model->whereHas('orders', function($query) {
                    $statuses = OrderStatus::getOrderStatusIdsByLabels([
                        OrderStatus::DRAFT, OrderStatus::OFFER, OrderStatus::PENDING, OrderStatus::CONFIRMED,
                    ]);
                    return $query->whereIn('order_status_id',$statuses);
                });
            }
        }

        return $model;
    }
}