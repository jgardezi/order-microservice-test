<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use App\Entities\OrderStatus;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserOfferCriteria implements CriteriaInterface {
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository) {
        if ($this->request->has('userID')) {
            $model = $model
                ->where(function ($query) {
                    $userId = $this->request->route('userId');
                    $query
                        ->where('author_id', '=', $userId)
                        ->orWhere('recipient_author_id', '=', $userId);
                })
                ->where('order_status_id', '=', OrderStatus::offer());
        }
        return $model;
    }
}