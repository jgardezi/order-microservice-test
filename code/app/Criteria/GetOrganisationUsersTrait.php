<?php
/**
 * Created by PhpStorm.
 * User: nigel
 * Date: 2019-03-06
 * Time: 12:06
 */

namespace App\Criteria;


trait GetOrganisationUsersTrait
{
    private function getOrganisationUserIds()
    {
        $rquser = $this->request->user();
        $users = $this->user_gateway->getOrganisationUsers($rquser->org_id);

        $ids=[];
        foreach ($users as $user) {
            $ids[] = $user->getId();
        }

        return $ids;
    }
}