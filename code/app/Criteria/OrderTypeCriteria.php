<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 1/4/19
 * Time: 12:36 PM
 */

namespace App\Criteria;


use App\Entities\OrderStatus;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderTypeCriteria implements CriteriaInterface {
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository) {
        if ($this->request->has('channelId')) {
            $model = $model->where('channel_id', '=', $this->request->get('channelId'));
        }

        if ($this->request->has('orderType')) {
            $orderType = OrderStatus::getByLabel($this->request->get('orderType'));
            $model = $model->where('order_status_id', '=', $orderType->id);
        }
        return $model;
    }
}