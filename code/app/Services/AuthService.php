<?php

namespace App\Services;

use Firebase\JWT\JWT;

class AuthService
{
    public function decodeJWT($jwt)
    {
        $publicKey = file_get_contents(env('TOKEN_PUBLIC_KEY'));
        $token = JWT::decode($jwt, $publicKey, ['RS256']);
        return $token;
    }
}
