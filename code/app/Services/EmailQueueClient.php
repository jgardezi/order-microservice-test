<?php

namespace App\Services;

use Aws\Exception\AwsException;

class EmailQueueClient implements EmailQueueClientInterface
{

    /**
     * @param string $messageDeduplicationId
     * @param string $messageGroupId
     * @param string $scope
     * @param array $args
     * @return mixed
     */
    public function send(string $messageDeduplicationId, string $messageGroupId, array $args)
    {
        $client = app()->make('aws')->createClient('sqs');
        $params = [
            'MessageDeduplicationId' => $messageDeduplicationId,
            'FifoQueue' => true,
            'MessageGroupId' => $messageGroupId,
            'MessageBody' => json_encode([
                "job" => "EMAIL_NOTIFICATION",
                "data" => $args
            ], true),
            'QueueUrl' => env('AWS_SQS_URL')
        ];

        try {
            $client->sendMessage($params);
        } catch (AwsException $e) {
        }
    }
}