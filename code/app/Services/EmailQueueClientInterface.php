<?php

namespace App\Services;

/**
 * Interface EmailQueueClientInterface
 * @package App\Services
 *
 * Message Deduplication ID
 * The token used for deduplication of sent messages. If a message with a particular message deduplication ID is sent successfully, any messages sent with the same message deduplication ID are accepted successfully but aren't delivered during the 5-minute deduplication interval.
 * Note
 * Message deduplication applies to an entire queue, not to individual message groups.
 *
 * Amazon SQS continues to keep track of the message deduplication ID even after the message is received and deleted.
 * Message Group ID
 * The tag that specifies that a message belongs to a specific message group. Messages that belong to the same message group are always processed one by one, in a strict order relative to the message group (however, messages that belong to different message groups might be processed out of order).
 */
interface EmailQueueClientInterface
{
    /**
     * @param string $messageDeduplicationId -- see detail description above, for keeping a message as unique, we can use instance type + instance id + timestamp, for example: ('order.' . $order->uuid . '.' . $triggering_time->toIso8601String())
     * @param string $messageGroupId -- see description above, we can use instance type + instance id, for example: ('order.' . $order->uuid)
     * @param array $args
     * args can contains:
     * isSystemEmail -- identify if this is an system email, if it's true, email service will use system email address and the sendFromEmail and senderID will be ignore
     * sendFromEmail -- the sender's email address, if be provided, the senderID will be ignore
     * sendToEmails -- the receiver's email address, if be provided, the receiverID will be ignore
     * senderID -- the sender's uuid
     * receiverIDs -- the receiver's uuid
     * data -- an array that contains the data payload for generating an email
     * scope -- what type of email, for example: 'order.proforma'
     * @return mixed
     */
    public function send(string $messageDeduplicationId, string $messageGroupId, array $args);
}