<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();


/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(\App\Supports\IncludesParser::class);

/*
|--------------------------------------------------------------------------
| App config
|--------------------------------------------------------------------------
*/
$app->configure('broadcasting');
$app->configure('database');
$app->configure('lada-cache');

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    \App\Http\Middleware\CorsMiddleware::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\EventServiceProvider::class);
$app->register(Illuminate\Broadcasting\BroadcastServiceProvider::class);
$app->register(App\Providers\BroadcastServiceProvider::class);
$app->register(App\Providers\ValidatorsProvider::class);
$app->register(Prettus\Repository\Providers\LumenRepositoryServiceProvider::class);
$app->register(App\Providers\PXCApiClientServiceProvider::class);
$app->register(App\Providers\SwisApiClientServiceProvider::class);
$app->register(Aws\Laravel\AwsServiceProvider::class);
$app->register(App\Providers\EmailNotificationServiceProvider::class);
$app->register(App\Providers\GatewayServiceProvider::class);
$app->register(Pearl\RequestValidate\RequestServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(Spiritix\LadaCache\LadaCacheServiceProvider::class);
/**
 * Interface to concrete bindings.
 */
$app->bind(App\Repositories\OrdersRepository::class, App\Repositories\OrdersRepositoryEloquent::class);
$app->bind(App\Repositories\OrderItemRepository::class, App\Repositories\OrderItemRepositoryEloquent::class);
$app->bind(App\Repositories\OrderStatusRepository::class, App\Repositories\OrderStatusRepositoryEloquent::class);
$app->bind(App\Repositories\OrderGroupRepository::class, App\Repositories\OrderGroupRepositoryEloquent::class);
$app->bind(App\Repositories\TermRepository::class, App\Repositories\TermRepositoryEloquent::class);
$app->bind(App\Repositories\ShippingRepository::class, App\Repositories\ShippingRepositoryEloquent::class);
/**
 * The application's route middleware.
 *
 * These middleware may be assigned to groups or used individually.
 */
$app->routeMiddleware([
    'jwt' => App\Http\Middleware\CheckJWT::class,
    'validate' => App\Http\Middleware\Validate::class,
    'permission' => App\Http\Middleware\Permission::class
]);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;
