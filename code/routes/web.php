<?php


// Don't change anything in this route. As its used by ALB for app health checking.
$router->get('/', 'HomeController@index');

$router->group([
    'prefix'     => 'orders',
    'middleware' => ['jwt'],
], function () use ($router) {
    $router->get('', [
        'as' => 'orders.index',
        'uses' => 'OrderController@index',
        'middleware' => ['validate:queryOrder']
    ]);
    $router->post('', [
        'as' => 'orders.store',
        'uses' => 'OrderController@store',
        'middleware' => ['validate:createOrder']
    ]);
    $router->get('offer', [
        'as' => 'offer.index',
        'uses' => 'OrderController@offers',
    ]);
    $router->get('summary', [
        'as' => 'order.summary',
        'uses' => 'OrderController@summary',
    ]);
    $router->post('broadcasting/check', [
        'uses' => 'BroadcastController@authenticate'
    ]);

    $router->group(['prefix' => '{id}'], function () use ($router) {
        $router->get('', [
            'as' => 'orders.show',
            'uses' => 'OrderController@show'
        ]);
        $router->patch('', [
            'as' => 'orders.update',
            'uses' => 'OrderController@update',
            'middleware' => ['validate:updateOrder']
        ]);
        $router->patch('touch', [
           'as' => 'orders.touch',
           'uses' => 'OrderController@touch'
        ]);
        $router->delete('', [
            'as' => 'orders.destroy',
            'uses' => 'OrderController@destroy',
            'middleware' => ['validate:deleteOrder']
        ]);
        $router->post('duplicate', [
            'as' => 'orders.duplicate',
            'uses' => 'OrderController@duplicate',
            'middleware' => ['validate:duplicateOrder']
        ]);
        $router->post('emailproforma', [
            'as' => 'orders.emailproforma',
            'uses' => 'OrderController@emailProforma',
        ]);

        $router->post('attachments', [
            'as' => 'orders.add_attachments',
            'uses' => 'OrderAttachmentController@store',
            'middleware' => ['permission:machine']
        ]);
        $router->delete('attachments', [
            'as' => 'orders.delete_attachments',
            'uses' => 'OrderAttachmentController@destroy',
            'middleware' => ['permission:machine']
        ]);

        $router->group(['prefix' => 'items'], function () use ($router) {
            $router->post('', ['as' => 'items.create', 'uses' => 'OrderItemsController@create']);
            $router->patch('', ['as' => 'items.update', 'uses' => 'OrderItemsController@update']);
            $router->delete('', ['as' => 'items.delete', 'uses' => 'OrderItemsController@delete']);
        });
    });
});

$router->group([
    'prefix'     => 'team',
    'middleware' => ['jwt'],
], function () use ($router) {
    $router->get('members', [
        'as' => 'team.index',
        'uses' => 'UserOrderController@myTeamMembers',
        'middleware' => ['permission:owner|manager']
    ]);

    $router->post('orders', [
        'as' => 'team.orders',
        'uses' => 'UserOrderController@myTeamOrders',
        'middleware' => ['permission:owner|manager']
    ]);
});

$router->group([
    'prefix'     => 'fields',
    'middleware' => ['jwt'],
], function () use ($router) {
    $router->get('terms', ['as' => 'orders.index','uses' => 'FieldsController@terms',]);
    $router->get('shipping', ['as' => 'orders.index','uses' => 'FieldsController@shipping',]);
});

$router->group([
    'prefix'     => 'users',
    'middleware' => ['jwt'],
], function () use ($router) {
    $router->group(['prefix' => '{userId}'], function () use ($router) {
        $router->get('orders', [
            'as' => 'user.orders.index',
            'uses' => 'UserOrderController@index',
            'middleware' => ['permission:owner|manager|machine']
        ]);

        $router->get('orders/offer', [
            'as' => 'user.offer.index',
            'uses' => 'UserOrderController@offers',
            'middleware' => ['permission:owner|manager|machine']
        ]);
    });
});