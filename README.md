# PrimeXConnect Order Microservice


## Description
This repository contains PrimeXConnect Order API Microservice.


## Authors
Name | Email | Role
--- | --- | ---
Javed Gardezi | [javed.gardezi@primexconnect.com](mailto:javed.gardezi@primexconnect.com) | Technical Team Lead
Kim Chen | [kim.chen@primexconnect.com](mailto:kim.chen@primexconnect.com) | Developer

## API URLs
* Local development: http://orders.local:82

## Requirements
* Git
* Docker >= 18.06.1-ce

## Installation

### 1. Clone this repository to your local machine
```bash
git clone git@bitbucket.org:primex-connect/order-microservice.git
```

### 2. Navigate to the project folder and then checkout the develop branch.
```bash
cd order-microservice
git checkout develop
```

### 3. Create an environment files
#### a. For docker environment file `docker/.env`
You do not need to change anything in `.env` file. Most of the configurations comes out of the box, if their are some issues contact author.
```bash
cp docker/env-example .env
```
#### b. For Lumen code environment file `code/.env`
```bash
cp code/.env.example .env
```

Update both `.env` files accordingly i.e. redis and mysql hosts and connection settings.

### 4. Start docker containers
The container that will be used for the development environment are
* `nginx`, `php-fpm`, `workspace` and `mysql`

The `workspace` and `php-fpm` will run automatically in most of the cases, so no need to specify them in the `up` command. 

For the first time installation it will take, please be patient.

Navigate to directory `./docker` 
```bash
docker-compose up -d nginx mysql
```

### 5. Verify running install
```bash
docker-compose ps

# The following container should be running i.e.

                 Name                                Command              State                    Ports
--------------------------------------------------------------------------------------------------------------------------
order-microservice_docker-in-docker_1   dockerd-entrypoint.sh           Up      2375/tcp
order-microservice_mysql_1              docker-entrypoint.sh mysqld     Up      0.0.0.0:3307->3306/tcp, 33060/tcp
order-microservice_nginx_1              /bin/bash /opt/startup.sh       Up      0.0.0.0:444->443/tcp, 0.0.0.0:82->80/tcp
order-microservice_php-fpm_1            docker-php-entrypoint php-fpm   Up      9000/tcp
order-microservice_workspace_1          /sbin/my_init                   Up      0.0.0.0:2224->22/tcp
``` 

### 6. Install Lumen App
#### a. Install composer dependencies
```bash
# From docker folder
docker-compose exec workspace composer install
```

#### b. Install composer dependencies
Get the current development env database. Import/create the database in mysql container and it can be accessible
outside the docker env on host `127.0.0.1:3306`.

### 7. Test API is running

In your fav web browser go to the following address to see API's are working 
- http://order.local:82/

### 8. Further reading
- [https://docs.sylius.com/en/1.3/book/products/products.html](Sylius Product) model represents unique products in your Sylius store. 
Every product can have different variations and attributes.
- [https://docs.sylius.com/en/1.3/book/products/inventory.html](Sylius Inventory) or 
- [https://packalyst.com/packages/package/stevebauman/inventory](Inventory Management Solution)
- Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs). 
- [http://laradock.io/](Laradock)

### 10.AWS ECS Cloud Hosting
- If you update `docker/.env`, please go to AWS console S3 and update the envivronment files for deployment.
  - Dev & Staging Orders service path: `s3://primex-assets/environment_files/orders-service/env.aws.dev`
  - Live/Production Orders service path: `s3://primex-assets/environment_files/orders-service/env.aws.prod`